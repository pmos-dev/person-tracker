import {
		commonsTypeIsString,
		commonsTypeIsDate,
		commonsTypeIsNumber,
		TPropertyObject
} from 'tscommons-es-core';
import { commonsBase62IsId } from 'tscommons-es-core';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { isECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { CommonsImportExport } from 'nodecommons-es-models-adamantine';

import { IData, INote } from 'person-tracker-ts-assets';

import { NamespaceModel, IInternalNamespace } from '../models/namespace.model';
import { FieldModel, IInternalField } from '../models/field.model';
import { EventModel, IInternalEvent } from '../models/event.model';
import { TriageModel, IInternalTriage } from '../models/triage.model';
import { StepModel, IInternalStep } from '../models/step.model';
import { FlowModel, IInternalFlow } from '../models/flow.model';
import { ConditionModel } from '../models/condition.model';
import { EventTriageModel } from '../models/event-triage.model';
import { PersonModel, IInternalPerson } from '../models/person.model';
import { InteractionModel, IInternalInteraction } from '../models/interaction.model';
import { PersonInteractionModel } from '../models/person-interaction.model';
import { InteractionStepModel } from '../models/interaction-step.model';
import { DataModel } from '../models/data.model';
import { NoteModel } from '../models/note.model';

export class Importer<
		DataI extends IData = IData,
		NoteI extends INote = INote,
		NamespaceModelM extends NamespaceModel = NamespaceModel,
		FieldModelM extends FieldModel = FieldModel,
		EventModelM extends EventModel = EventModel,
		TriageModelM extends TriageModel = TriageModel,
		StepModelM extends StepModel = StepModel,
		FlowModelM extends FlowModel = FlowModel,
		ConditionModelM extends ConditionModel = ConditionModel,
		EventTriageModelM extends EventTriageModel = EventTriageModel,
		PersonModelM extends PersonModel = PersonModel,
		InteractionModelM extends InteractionModel = InteractionModel,
		PersonInteractionModelM extends PersonInteractionModel = PersonInteractionModel,
		InteractionStepModelM extends InteractionStepModel = InteractionStepModel,
		DataModelM extends DataModel<DataI> = DataModel<DataI>,
		NoteModelM extends NoteModel<NoteI> = NoteModel<NoteI>
> extends CommonsImportExport {
	protected lastStep: IInternalStep|undefined;
	protected lastFlow: IInternalFlow|undefined;
	
	constructor(
			protected prefix: string,
			protected namespaceModel: NamespaceModelM,
			protected fieldModel: FieldModelM,
			protected eventModel: EventModelM,
			protected triageModel: TriageModelM,
			protected stepModel: StepModelM,
			protected flowModel: FlowModelM,
			protected conditionModel: ConditionModelM,
			protected eventTriageModel: EventTriageModelM,
			protected personModel: PersonModelM,
			protected interactionModel: InteractionModelM,
			protected personInteractionModel: PersonInteractionModelM,
			protected interactionStepModel: InteractionStepModelM,
			protected dataModel: DataModelM,
			protected noteModel: NoteModelM,
			private extendDataImport: (
					event: IInternalEvent,
					fields: TPropertyObject<any>,
					line: string
			) => Promise<boolean>,
			private extendNoteImport: (
					event: IInternalEvent,
					fields: TPropertyObject<any>,
					line: string
			) => Promise<boolean>
	) {
		super();
	}
	
	protected async importField(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing field');

		return await this.importManagedOrientatedOrdered<
				IInternalField,
				IInternalNamespace
		>(
				this.fieldModel,
				this.namespaceModel,
				namespace,
				line
		);
	}
	
	protected async importEvent(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing event');

		return await this.importManagedSecondClass<
				IInternalEvent,
				IInternalNamespace
		>(
				this.eventModel,
				this.namespaceModel,
				namespace,
				line
		);
	}
	
	protected async importTriage(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing triage');

		return await this.importManagedSecondClass<
				IInternalTriage,
				IInternalNamespace
		>(
				this.triageModel,
				this.namespaceModel,
				namespace,
				line
		);
	}
	
	protected async importStep(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing step');

		const triageName: unknown|undefined = CommonsImportExport.extractField('TRIAGE', line);
		if (!commonsTypeIsString(triageName)) return false;
		const triage: IInternalTriage|undefined = await this.triageModel.getByName(namespace, triageName);
		if (!triage) return false;

		const uid: unknown|undefined = CommonsImportExport.extractField('UID', line);
		if (!commonsTypeIsString(uid) || !commonsBase62IsId(uid)) return false;

		const type: unknown|undefined = CommonsImportExport.extractField('TYPE', line);
		if (!isECommonsStepFlowStepType(type)) return false;

		try {
			this.lastStep = await this.stepModel.insertForFirstClass(
					triage,
					{
							uid: uid,
							type: type
					}
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}
	
	protected async importStepField(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing step field');

		if (!this.lastStep) return false;

		const fieldName: unknown|undefined = CommonsImportExport.extractField('FIELD', line);
		if (!commonsTypeIsString(fieldName)) return false;
		const field: IInternalField|undefined = await this.fieldModel.getByName(namespace, fieldName);
		if (!field) return false;

		this.lastStep.field = field.id;

		try {
			await this.stepModel.update(this.lastStep);
			
			return true;
		} catch (e) {
			return false;
		}
	}
	
	protected async importFlow(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing flow');

		const triageName: unknown|undefined = CommonsImportExport.extractField('TRIAGE', line);
		if (!commonsTypeIsString(triageName)) return false;
		const triage: IInternalTriage|undefined = await this.triageModel.getByName(namespace, triageName);
		if (!triage) return false;

		const stepUid: unknown|undefined = CommonsImportExport.extractField('STEP', line);
		if (!commonsTypeIsString(stepUid) || !commonsBase62IsId(stepUid)) return false;
		const step: IInternalStep|undefined = await this.stepModel.getByUid(stepUid);
		if (!step) return false;

		const outStepUid: unknown|undefined = CommonsImportExport.extractField('OUTSTEP', line);
		if (!commonsTypeIsString(outStepUid) || !commonsBase62IsId(outStepUid)) return false;
		const outStep: IInternalStep|undefined = await this.stepModel.getByUid(outStepUid);
		if (!outStep) return false;

		try {
			this.lastFlow = await this.flowModel.insertForFirstClass(
					step,
					{
							outstep: outStep.id
					}
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}
	
	protected async importCondition(
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing condition');

		if (!this.lastFlow) return false;

		let value: string|undefined;

		if (CommonsImportExport.hasAttribute(line, 'NONE')) {
			value = undefined;
		} else {
			// the condition.value is already json encoded

			const temp: unknown|undefined = CommonsImportExport.extractField('VALUE', line);
			if (!commonsTypeIsString(temp)) return false;
			
			value = temp;
		}

		try {
			await this.conditionModel.insertForFirstClass(
					this.lastFlow,
					{
							value: value
					}
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}

	protected async importEventTriage(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing event-triage');

		return await CommonsImportExport.importM2MLinkTable<
				ICommonsM2MLink<IInternalEvent, IInternalTriage>,
				IInternalEvent,
				IInternalTriage,
				IInternalNamespace
		>(
				this.eventTriageModel,
				this.namespaceModel,
				namespace,
				line
		);
	}
	
	protected async importPerson(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing person');

		const eventName: unknown|undefined = CommonsImportExport.extractField('EVENT', line);
		if (!commonsTypeIsString(eventName)) return false;
		const event: IInternalEvent|undefined = await this.eventModel.getByName(namespace, eventName);
		if (!event) return false;

		const uid: unknown|undefined = CommonsImportExport.extractField('UID', line);
		if (!commonsTypeIsString(uid) || !commonsBase62IsId(uid)) return false;

		try {
			await this.personModel.insertForFirstClass(
					event,
					{
							uid: uid
					}
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}
	
	protected async importInteraction(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing interaction');

		const eventName: unknown|undefined = CommonsImportExport.extractField('EVENT', line);
		if (!commonsTypeIsString(eventName)) return false;
		const event: IInternalEvent|undefined = await this.eventModel.getByName(namespace, eventName);
		if (!event) return false;

		const uid: unknown|undefined = CommonsImportExport.extractField('UID', line);
		if (!commonsTypeIsString(uid) || !commonsBase62IsId(uid)) return false;

		try {
			await this.interactionModel.insertForFirstClass(
					event,
					{
							uid: uid
					}
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}

	protected async importPersonInteraction(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing person-interaction');

		const eventName: unknown|undefined = CommonsImportExport.extractField('EVENT', line);
		if (!commonsTypeIsString(eventName)) return false;
		const event: IInternalEvent|undefined = await this.eventModel.getByName(namespace, eventName);
		if (!event) return false;

		const personUid: unknown|undefined = CommonsImportExport.extractField('PERSON', line);
		if (!commonsTypeIsString(personUid) || !commonsBase62IsId(personUid)) return false;
		const person: IInternalPerson|undefined = await this.personModel.getByUid(personUid);
		if (!person) return false;
		if (person.event !== event.id) return false;

		const interactionUid: unknown|undefined = CommonsImportExport.extractField('INTERACTION', line);
		if (!commonsTypeIsString(interactionUid) || !commonsBase62IsId(interactionUid)) return false;
		const interaction: IInternalInteraction|undefined = await this.interactionModel.getByUid(interactionUid);
		if (!interaction) return false;
		if (interaction.event !== event.id) return false;

		try {
			await this.personInteractionModel.link(
					person,
					interaction
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}

	protected async importInteractionStep(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing interaction-step');

		const eventName: unknown|undefined = CommonsImportExport.extractField('EVENT', line);
		if (!commonsTypeIsString(eventName)) return false;
		const event: IInternalEvent|undefined = await this.eventModel.getByName(namespace, eventName);
		if (!event) return false;

		const interactionUid: unknown|undefined = CommonsImportExport.extractField('INTERACTION', line);
		if (!commonsTypeIsString(interactionUid) || !commonsBase62IsId(interactionUid)) return false;
		const interaction: IInternalInteraction|undefined = await this.interactionModel.getByUid(interactionUid);
		if (!interaction) return false;
		if (interaction.event !== event.id) return false;

		const triages: IInternalTriage[] = await this.eventTriageModel.listBsByA(event);
		if (triages.length === 0) return false;

		const triage: IInternalTriage = triages[0];

		const stepUid: unknown|undefined = CommonsImportExport.extractField('STEP', line);
		if (!commonsTypeIsString(stepUid) || !commonsBase62IsId(stepUid)) return false;
		const step: IInternalStep|undefined = await this.stepModel.getByUid(stepUid);
		if (!step) return false;
		if (step.triage !== triage.id) return false;

		const timestamp: unknown = CommonsImportExport.extractField('TIMESTAMP', line);
		if (!commonsTypeIsDate(timestamp)) return false;

		try {
			await this.interactionStepModel.createForInteraction(
					interaction,
					step,
					timestamp
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}

	protected async importData(
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing data');

		const uid: unknown|undefined = CommonsImportExport.extractField('UID', line);
		if (!commonsTypeIsString(uid) || !commonsBase62IsId(uid)) return false;

		const logNo: unknown|undefined = CommonsImportExport.extractField('LOGNO', line);
		if (!commonsTypeIsNumber(logNo)) return false;

		const eventUid: unknown|undefined = CommonsImportExport.extractField('EVENT', line);
		if (!commonsTypeIsString(eventUid) || !commonsBase62IsId(eventUid)) return false;
		const event: IInternalEvent|undefined = await this.eventModel.getByUid(eventUid);
		if (!event) return false;

		const fieldUid: unknown|undefined = CommonsImportExport.extractField('FIELD', line);
		if (!commonsTypeIsString(fieldUid) || !commonsBase62IsId(fieldUid)) return false;
		const field: IInternalField|undefined = await this.fieldModel.getByUid(fieldUid);
		if (!field) return false;

		let person: IInternalPerson|undefined;
		let interaction: IInternalInteraction|undefined;

		const personUid: unknown|undefined = CommonsImportExport.extractField('PERSON', line);
		if (commonsTypeIsString(personUid) && commonsBase62IsId(personUid)) {
			person = await this.personModel.getByUid(personUid);
			if (!person) return false;
		}

		const interactionUid: unknown|undefined = CommonsImportExport.extractField('INTERACTION', line);
		if (commonsTypeIsString(interactionUid) && commonsBase62IsId(interactionUid)) {
			interaction = await this.interactionModel.getByUid(interactionUid);
			if (!interaction) return false;
		}
		
		if (!person && !interaction) return false;
		
		const timestamp: unknown = CommonsImportExport.extractField('TIMESTAMP', line);
		if (!commonsTypeIsDate(timestamp)) return false;
		
		const value: unknown|undefined = CommonsImportExport.extractField('VALUE', line);
		if (!commonsTypeIsString(value)) return false;

		const parsed: unknown = JSON.parse(value);

		const fields: TPropertyObject<any> = {
				uid: uid,
				logno: logNo,
				field: field.id,
				person: person ? person.id : undefined,
				interaction: interaction ? interaction.id : undefined,
				timestamp: timestamp,
				value: parsed
		};

		if (this.extendDataImport) {
			if (!(await this.extendDataImport(
					event,
					fields,
					line
			))) return false;
		}

		try {
			await this.dataModel.insertForFirstClass(
					event,
					fields
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}

	protected async importNote(
			line: string
	): Promise<boolean> {
		commonsOutputDebug('Importing note');

		const uid: unknown|undefined = CommonsImportExport.extractField('UID', line);
		if (!commonsTypeIsString(uid) || !commonsBase62IsId(uid)) return false;

		const logNo: unknown|undefined = CommonsImportExport.extractField('LOGNO', line);
		if (!commonsTypeIsNumber(logNo)) return false;

		const eventUid: unknown|undefined = CommonsImportExport.extractField('EVENT', line);
		if (!commonsTypeIsString(eventUid) || !commonsBase62IsId(eventUid)) return false;
		const event: IInternalEvent|undefined = await this.eventModel.getByUid(eventUid);
		if (!event) return false;

		let person: IInternalPerson|undefined;
		let interaction: IInternalInteraction|undefined;

		const personUid: unknown|undefined = CommonsImportExport.extractField('PERSON', line);
		if (commonsTypeIsString(personUid) && commonsBase62IsId(personUid)) {
			person = await this.personModel.getByUid(personUid);
			if (!person) return false;
		}

		const interactionUid: unknown|undefined = CommonsImportExport.extractField('INTERACTION', line);
		if (commonsTypeIsString(interactionUid) && commonsBase62IsId(interactionUid)) {
			interaction = await this.interactionModel.getByUid(interactionUid);
			if (!interaction) return false;
		}
		
		if (!person && !interaction) return false;
		
		const timestamp: unknown = CommonsImportExport.extractField('TIMESTAMP', line);
		if (!commonsTypeIsDate(timestamp)) return false;
		
		const value: unknown|undefined = CommonsImportExport.extractField('VALUE', line);
		if (!commonsTypeIsString(value)) return false;

		const fields: TPropertyObject<any> = {
				uid: uid,
				logno: logNo,
				person: person ? person.id : undefined,
				interaction: interaction ? interaction.id : undefined,
				timestamp: timestamp,
				value: value
		};

		if (this.extendNoteImport) {
			if (!(await this.extendNoteImport(
					event,
					fields,
					line
			))) return false;
		}

		try {
			await this.noteModel.insertForFirstClass(
					event,
					fields
			);
			
			return true;
		} catch (e) {
			return false;
		}
	}
	
	protected buildRegex(verb: string): RegExp {
		return new RegExp(`^${this.prefix} ${verb} `);
	}
	
	public async importNamespaceLine(
			namespace: IInternalNamespace,
			line: string
	): Promise<boolean> {
		if (this.buildRegex('CREATE FIELD').test(line.trim())) {
			return await this.importField(namespace, line);
		}
		if (this.buildRegex('CREATE EVENT').test(line.trim())) {
			return await this.importEvent(namespace, line);
		}
		if (this.buildRegex('CREATE TRIAGE').test(line.trim())) {
			return await this.importTriage(namespace, line);
		}
		if (this.buildRegex('CREATE STEP').test(line.trim())) {
			return await this.importStep(namespace, line);
		}
		if (this.buildRegex('SET FIELD FOR LAST STEP').test(line.trim())) {
			return await this.importStepField(namespace, line);
		}
		if (this.buildRegex('CREATE FLOW').test(line.trim())) {
			return await this.importFlow(namespace, line);
		}
		if (this.buildRegex('ADD CONDITION TO LAST FLOW').test(line.trim())) {
			return await this.importCondition(line);
		}
		if (this.buildRegex('LINK EVENTTRIAGE').test(line.trim())) {
			return await this.importEventTriage(namespace, line);
		}
		if (this.buildRegex('CREATE PERSON').test(line.trim())) {
			return await this.importPerson(namespace, line);
		}
		if (this.buildRegex('CREATE INTERACTION').test(line.trim())) {
			return await this.importInteraction(namespace, line);
		}
		if (this.buildRegex('LINK PERSONINTERACTION').test(line.trim())) {
			return await this.importPersonInteraction(namespace, line);
		}
		if (this.buildRegex('CREATE INTERACTIONSTEP').test(line.trim())) {
			return await this.importInteractionStep(namespace, line);
		}
		if (this.buildRegex('CREATE DATA').test(line.trim())) {
			return await this.importData(line);
		}
		if (this.buildRegex('CREATE NOTE').test(line.trim())) {
			return await this.importNote(line);
		}
		
		return false;
	}
}
