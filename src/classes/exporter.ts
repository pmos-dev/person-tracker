import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import { ECommonsImportExportEncoding, TCommonsImportExportValues } from 'nodecommons-es-models';
import { CommonsImportExport } from 'nodecommons-es-models-adamantine';

import { IData, INote } from 'person-tracker-ts-assets';

import { NamespaceModel, IInternalNamespace } from '../models/namespace.model';
import { FieldModel, IInternalField } from '../models/field.model';
import { EventModel, IInternalEvent } from '../models/event.model';
import { TriageModel, IInternalTriage } from '../models/triage.model';
import { StepModel, IInternalStep } from '../models/step.model';
import { FlowModel, IInternalFlow } from '../models/flow.model';
import { ConditionModel, IInternalCondition } from '../models/condition.model';
import { EventTriageModel } from '../models/event-triage.model';
import { PersonModel, IInternalPerson } from '../models/person.model';
import { InteractionModel, IInternalInteraction } from '../models/interaction.model';
import { PersonInteractionModel } from '../models/person-interaction.model';
import { IInternalInteractionStep, InteractionStepModel } from '../models/interaction-step.model';
import { DataModel, IInternalData, IInternalInteractionData, IInternalPersonData } from '../models/data.model';
import { IInternalInteractionNote, IInternalNote, IInternalPersonNote, NoteModel } from '../models/note.model';

export class Exporter<
		DataI extends IData = IData,
		NoteI extends INote = INote,
		NamespaceModelM extends NamespaceModel = NamespaceModel,
		FieldModelM extends FieldModel = FieldModel,
		EventModelM extends EventModel = EventModel,
		TriageModelM extends TriageModel = TriageModel,
		StepModelM extends StepModel = StepModel,
		FlowModelM extends FlowModel = FlowModel,
		ConditionModelM extends ConditionModel = ConditionModel,
		EventTriageModelM extends EventTriageModel = EventTriageModel,
		PersonModelM extends PersonModel = PersonModel,
		InteractionModelM extends InteractionModel = InteractionModel,
		PersonInteractionModelM extends PersonInteractionModel = PersonInteractionModel,
		InteractionStepModelM extends InteractionStepModel = InteractionStepModel,
		DataModelM extends DataModel<DataI> = DataModel<DataI>,
		NoteModelM extends NoteModel<NoteI> = NoteModel<NoteI>
> extends CommonsImportExport {
	protected out: string[] = [];
	
	constructor(
			protected prefix: string,
			protected namespaceModel: NamespaceModelM,
			protected fieldModel: FieldModelM,
			protected eventModel: EventModelM,
			protected triageModel: TriageModelM,
			protected stepModel: StepModelM,
			protected flowModel: FlowModelM,
			protected conditionModel: ConditionModelM,
			protected eventTriageModel: EventTriageModelM,
			protected personModel: PersonModelM,
			protected interactionModel: InteractionModelM,
			protected personInteractionModel: PersonInteractionModelM,
			protected interactionStepModel: InteractionStepModelM,
			protected dataModel: DataModelM,
			protected noteModel: NoteModelM,
			private extendDataExport: (
					namespace: IInternalNamespace,
					event: IInternalEvent,
					field: IInternalField,
					data: IInternalData<DataI>,
					fields: TCommonsImportExportValues,
					attributes: string[]
			) => Promise<void>,
			private extendNoteExport: (
					namespace: IInternalNamespace,
					event: IInternalEvent,
					note: IInternalNote<NoteI>,
					fields: TCommonsImportExportValues,
					attributes: string[]
			) => Promise<void>
	) {
		super();
	}
	
	protected async exportFields(namespace: IInternalNamespace): Promise<void> {
		await CommonsImportExport.exportManagedOrientatedOrdered<
				IInternalField,
				IInternalNamespace
		>(
				this.prefix,
				this.fieldModel,
				'FIELD',
				[ namespace ],
				this.namespaceModel,
				{},
				this.out
		);
	}
	
	protected async exportEvents(namespace: IInternalNamespace): Promise<void> {
		await CommonsImportExport.exportManagedSecondClass<
				IInternalEvent,
				IInternalNamespace
		>(
				this.prefix,
				this.eventModel,
				'EVENT',
				[ namespace ],
				this.namespaceModel,
				{},
				this.out
		);
	}

	protected async exportTriages(namespace: IInternalNamespace): Promise<void> {
		await CommonsImportExport.exportManagedSecondClass<
				IInternalTriage,
				IInternalNamespace
		>(
				this.prefix,
				this.triageModel,
				'TRIAGE',
				[ namespace ],
				this.namespaceModel,
				{},
				this.out
		);

		const triages: IInternalTriage[] = await this.triageModel.listByFirstClass(namespace);
		for (const triage of triages) {
			const steps: IInternalStep[] = await this.stepModel.listByFirstClass(triage);
		
			// need to export all the steps prior to flows, as the flows can be circular
			for (const step of steps) {
				this.out.push(CommonsImportExport.writeLine(
						`${this.prefix} CREATE STEP`,
						{
								TRIAGE: {
										encoding: ECommonsImportExportEncoding.SIMPLE,
										value: triage.name
								},
								UID: {
										encoding: ECommonsImportExportEncoding.BASE62ID,
										value: step.uid
								},
								TYPE: {
										encoding: ECommonsImportExportEncoding.SIMPLE,
										value: step.type
								}
						}
				));

				if (step.type === ECommonsStepFlowStepType.DATA) {
					if (!step.field) throw new Error('Data step does not have an associated field. This should not be possible.');
					
					const field: IInternalField|undefined = await this.fieldModel.getByFirstClassAndId(namespace, step.field);
					if (!field) throw new Error('Missing field foreign key for step');

					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} SET FIELD FOR LAST STEP`,
							{
									FIELD: {
											encoding: ECommonsImportExportEncoding.SIMPLE,
											value: field.name
									}
							}
					));
				}
			}

			for (const step of steps) {
				const flows: IInternalFlow[] = await this.flowModel.listOrderedByFirstClass(step);
				for (const flow of flows) {
					const outstep: IInternalStep|undefined = await this.stepModel.getByFirstClassAndId(triage, flow.outstep);
					if (!outstep) throw new Error('No such outstep exists. This should not be possible due to foreign keys');

					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} CREATE FLOW`,
							{
									TRIAGE: {
											encoding: ECommonsImportExportEncoding.SIMPLE,
											value: triage.name
									},
									STEP: {
											encoding: ECommonsImportExportEncoding.BASE62ID,
											value: step.uid
									},
									OUTSTEP: {
											encoding: ECommonsImportExportEncoding.BASE62ID,
											value: outstep.uid
									}
							}
					));
					
					// Can't use exportManagedOrientatedOrdered even though this is a managed class, as its Step/Flow parents are not managed
					const conditions: IInternalCondition[] = await this.conditionModel.listByFirstClass(flow);
					for (const condition of conditions) {
						// the condition.value is already json encoded
						
						if (condition.value === undefined) {
							this.out.push(CommonsImportExport.writeLine(
									`${this.prefix} ADD CONDITION TO LAST FLOW`,
									{},
									[ 'NONE' ]
							));
						} else {
							this.out.push(CommonsImportExport.writeLine(
									`${this.prefix} ADD CONDITION TO LAST FLOW`,
									{
											VALUE: {
													encoding: ECommonsImportExportEncoding.HEX,
													value: condition.value || ''
											}
									}
							));
						}
					}
				}
			}
		}
	}

	protected async exportEventTriages(namespace: IInternalNamespace): Promise<void> {
		await CommonsImportExport.exportM2MLinkTable<
				ICommonsM2MLink<IInternalEvent, IInternalTriage>,
				IInternalEvent,
				IInternalTriage,
				IInternalNamespace
		>(
				this.prefix,
				this.eventTriageModel,
				'EVENTTRIAGE',
				[ namespace ],
				[ namespace ],
				this.namespaceModel,
				this.out
		);
	}

	protected async exportPersons(namespace: IInternalNamespace): Promise<void> {
		const events: IInternalEvent[] = await this.eventModel.listByFirstClass(namespace);
		for (const event of events) {
			const persons: IInternalPerson[] = await this.personModel.listByFirstClass(event);
			for (const person of persons) {
				this.out.push(CommonsImportExport.writeLine(
						`${this.prefix} CREATE PERSON`,
						{
								UID: {
										encoding: ECommonsImportExportEncoding.BASE62ID,
										value: person.uid
								},
								EVENT: {
										encoding: ECommonsImportExportEncoding.SIMPLE,
										value: event.name
								}
						}
				));
			}
		}
	}

	protected async exportInteractions(namespace: IInternalNamespace): Promise<void> {
		const events: IInternalEvent[] = await this.eventModel.listByFirstClass(namespace);
		for (const event of events) {
			const interactions: IInternalInteraction[] = await this.interactionModel.listByFirstClass(event);
			for (const interaction of interactions) {
				this.out.push(CommonsImportExport.writeLine(
						`${this.prefix} CREATE INTERACTION`,
						{
								UID: {
										encoding: ECommonsImportExportEncoding.BASE62ID,
										value: interaction.uid
								},
								EVENT: {
										encoding: ECommonsImportExportEncoding.SIMPLE,
										value: event.name
								}
						}
				));
			}
		}
	}

	protected async exportPersonInteractions(namespace: IInternalNamespace): Promise<void> {
		const events: IInternalEvent[] = await this.eventModel.listByFirstClass(namespace);
		for (const event of events) {
			const persons: IInternalPerson[] = await this.personModel.listByFirstClass(event);
			for (const person of persons) {
				const interactions: IInternalInteraction[] = await this.personInteractionModel.listBsByA(person);
				for (const interaction of interactions) {
					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} LINK PERSONINTERACTION`,
							{
									PERSON: {
											encoding: ECommonsImportExportEncoding.BASE62ID,
											value: person.uid
									},
									INTERACTION: {
											encoding: ECommonsImportExportEncoding.BASE62ID,
											value: interaction.uid
									},
									EVENT: {
											encoding: ECommonsImportExportEncoding.SIMPLE,
											value: event.name
									}
							}
					));
				}
			}
		}
	}

	protected async exportInteractionSteps(namespace: IInternalNamespace): Promise<void> {
		const events: IInternalEvent[] = await this.eventModel.listByFirstClass(namespace);
		for (const event of events) {
			const interactions: IInternalInteraction[] = await this.interactionModel.listByFirstClass(event);
			for (const interaction of interactions) {
				const interactionSteps: IInternalInteractionStep[] = await this.interactionStepModel.listByInteraction(interaction);

				for (const interactionStep of interactionSteps) {
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(interactionStep.step);
					if (!step) continue;

					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} CREATE INTERACTIONSTEP`,
							{
									EVENT: {
											encoding: ECommonsImportExportEncoding.SIMPLE,
											value: event.name
									},
									INTERACTION: {
											encoding: ECommonsImportExportEncoding.BASE62ID,
											value: interaction.uid
									},
									STEP: {
											encoding: ECommonsImportExportEncoding.BASE62ID,
											value: step.uid
									},
									TIMESTAMP: {
											encoding: ECommonsImportExportEncoding.DATETIME,
											value: interactionStep.timestamp
									}
							}
					));
				}
			}
		}
	}

	protected async exportDatas(namespace: IInternalNamespace): Promise<void> {
		const events: IInternalEvent[] = await this.eventModel.listByFirstClass(namespace);
		for (const event of events) {
			const datas: IInternalData<DataI>[] = await this.dataModel.listByFirstClass(event);
			
			for (const data of datas) {
				const field: IInternalField|undefined = await this.fieldModel.getByFirstClassAndId(namespace, data.field);
				if (!field) throw new Error('No such field exists. This should not be possible due to referential integrity');

				if (commonsTypeHasPropertyNumber(data, 'person')) {
					const typecast: IInternalPersonData<DataI> = data as IInternalPersonData<DataI>;
					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(typecast.person);
					if (!person) throw new Error('Invalid data person');

					const fields: TCommonsImportExportValues = {
							UID: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: typecast.uid
							},
							LOGNO: {
									encoding: ECommonsImportExportEncoding.INT,
									value: typecast.logno
							},
							EVENT: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: event.uid
							},
							FIELD: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: field.uid
							},
							PERSON: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: person.uid
							},
							TIMESTAMP: {
									encoding: ECommonsImportExportEncoding.DATETIME,
									value: typecast.timestamp
							},
							VALUE: {
									encoding: ECommonsImportExportEncoding.HEX,
									value: JSON.stringify(typecast.value)
							}
					};

					const attributes: string[] = [];

					if (this.extendDataExport) {
						await this.extendDataExport(
								namespace,
								event,
								field,
								data,
								fields,
								attributes
						);
					}
					
					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} CREATE DATA`,
							fields,
							attributes
					));
				}

				if (commonsTypeHasPropertyNumber(data, 'interaction')) {
					const typecast: IInternalInteractionData<DataI> = data as IInternalInteractionData<DataI>;
					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(typecast.interaction);
					if (!interaction) throw new Error('Invalid data interaction');
					
					const fields: TCommonsImportExportValues = {
							UID: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: typecast.uid
							},
							LOGNO: {
									encoding: ECommonsImportExportEncoding.INT,
									value: typecast.logno
							},
							EVENT: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: event.uid
							},
							FIELD: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: field.uid
							},
							INTERACTION: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: interaction.uid
							},
							TIMESTAMP: {
									encoding: ECommonsImportExportEncoding.DATETIME,
									value: typecast.timestamp
							},
							VALUE: {
									encoding: ECommonsImportExportEncoding.HEX,
									value: JSON.stringify(typecast.value)
							}
					};

					const attributes: string[] = [];

					if (this.extendDataExport) {
						await this.extendDataExport(
								namespace,
								event,
								field,
								data,
								fields,
								attributes
						);
					}
					
					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} CREATE DATA`,
							fields,
							attributes
					));
				}
			}
		}
	}

	protected async exportNotes(namespace: IInternalNamespace): Promise<void> {
		const events: IInternalEvent[] = await this.eventModel.listByFirstClass(namespace);
		for (const event of events) {
			const notes: IInternalNote<NoteI>[] = await this.noteModel.listByFirstClass(event);
			
			for (const note of notes) {
				if (commonsTypeHasPropertyNumber(note, 'person')) {
					const typecast: IInternalPersonNote<NoteI> = note as IInternalPersonNote<NoteI>;
					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(typecast.person);
					if (!person) throw new Error('Invalid note person');

					const fields: TCommonsImportExportValues = {
							UID: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: typecast.uid
							},
							LOGNO: {
									encoding: ECommonsImportExportEncoding.INT,
									value: typecast.logno
							},
							EVENT: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: event.uid
							},
							PERSON: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: person.uid
							},
							TIMESTAMP: {
									encoding: ECommonsImportExportEncoding.DATETIME,
									value: typecast.timestamp
							},
							VALUE: {
									encoding: ECommonsImportExportEncoding.HEX,
									value: JSON.stringify(typecast.value)
							}
					};
					
					const attributes: string[] = [];

					if (this.extendNoteExport) {
						await this.extendNoteExport(
								namespace,
								event,
								note,
								fields,
								attributes
						);
					}
					
					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} CREATE NOTE`,
							fields,
							attributes
					));
				}

				if (commonsTypeHasPropertyNumber(note, 'interaction')) {
					const typecast: IInternalInteractionNote<NoteI> = note as IInternalInteractionNote<NoteI>;
					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(typecast.interaction);
					if (!interaction) throw new Error('Invalid note interaction');
					
					const fields: TCommonsImportExportValues = {
							UID: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: typecast.uid
							},
							LOGNO: {
									encoding: ECommonsImportExportEncoding.INT,
									value: typecast.logno
							},
							EVENT: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: event.uid
							},
							INTERACTION: {
									encoding: ECommonsImportExportEncoding.BASE62ID,
									value: interaction.uid
							},
							TIMESTAMP: {
									encoding: ECommonsImportExportEncoding.DATETIME,
									value: typecast.timestamp
							},
							VALUE: {
									encoding: ECommonsImportExportEncoding.HEX,
									value: JSON.stringify(typecast.value)
							}
					};
					
					const attributes: string[] = [];

					if (this.extendNoteExport) {
						await this.extendNoteExport(
								namespace,
								event,
								note,
								fields,
								attributes
						);
					}
					
					this.out.push(CommonsImportExport.writeLine(
							`${this.prefix} CREATE NOTE`,
							fields,
							attributes
					));
				}
			}
		}
	}

	public reset(): void {
		this.out = [];
	}
	
	public getExport(): string[] {
		return this.out.slice();
	}
	
	public async exportNamespace(namespace: IInternalNamespace): Promise<void> {
		await this.exportFields(namespace);
		
		await this.exportEvents(namespace);
		await this.exportTriages(namespace);
		await this.exportEventTriages(namespace);

		await this.exportPersons(namespace);
		await this.exportInteractions(namespace);
		await this.exportPersonInteractions(namespace);

		await this.exportDatas(namespace);
		await this.exportNotes(namespace);

		await this.exportInteractionSteps(namespace);
	}
}
