/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import { ICondition, TStepFlowRenderedFlow, TStepFlowRenderedStep, TStepFlowRenderedTriage } from 'person-tracker-ts-assets';

import { IInternalTriage } from '../models/triage.model';
import { StepModel, IInternalStep } from '../models/step.model';
import { FlowModel, IInternalFlow } from '../models/flow.model';
import { ConditionModel } from '../models/condition.model';
import { FieldModel, IInternalField } from '../models/field.model';

// non-recursive version to prevent infinite loops

export class StepFlowRenderer {
	constructor(
			private stepModel: StepModel,
			private flowModel: FlowModel,
			private conditionModel: ConditionModel,
			private fieldModel: FieldModel
	) {}

	private async renderFlows(step: IInternalStep): Promise<TStepFlowRenderedFlow[]> {
		const flows: IInternalFlow[] = await this.flowModel.listOrderedByFirstClass(step);

		const rendereds: TStepFlowRenderedFlow[] = [];

		for (const flow of flows) {
			const conditions: ICondition[] = await this.conditionModel.listByFlow(flow);

			if (conditions.length === 0) {
				// switch default
				const existing: TStepFlowRenderedFlow|undefined = rendereds
						.find((rendered: TStepFlowRenderedFlow): boolean => rendered.value === null);
				if (existing) throw new Error(`Multiple default routes for step flow ${flow.id}. This is not possible.`);

				const outstep: IInternalStep|undefined = await this.stepModel.getByIdOnly(flow.outstep);
				if (!outstep) throw new Error('Missing outstep. Foreign keys should make this impossible');

				rendereds.push({
						value: null,
						outstep: outstep.uid
				});
			}

			for (const condition of conditions) {
				const validValue: number|string|boolean|null = condition.value === undefined ? null : condition.value;

				const existing: TStepFlowRenderedFlow|undefined = rendereds
						.find((rendered: TStepFlowRenderedFlow): boolean => rendered.value === validValue);
				if (existing) throw new Error(`Multiple default routes for step flow ${flow.id}. This is not possible.`);

				const outstep: IInternalStep|undefined = await this.stepModel.getByIdOnly(flow.outstep);
				if (!outstep) throw new Error('Missing outstep. Foreign keys should make this impossible');

				rendereds.push({
						value: validValue,
						outstep: outstep.uid
				});
			}
		}

		return rendereds;
	}

	private async renderStep(step: IInternalStep): Promise<TStepFlowRenderedStep> {
		switch (step.type) {
			case ECommonsStepFlowStepType.COMPLETE:
			case ECommonsStepFlowStepType.ABORT:
				return {
						type: step.type,
						uid: step.uid,
						flows: [],
						field: null
				};
		}

		const flows: TStepFlowRenderedFlow[] = await this.renderFlows(step);
		if (flows.length === 0) {
			// implicit abort
			return {
					uid: step.uid,
					type: ECommonsStepFlowStepType.ABORT,
					flows: [],
					field: null
			};
		}
		
		if (!step.field) {
			// noop step, possibly for things like flow from the root to first step etc.
			// multiple flows cannot be possible, so just use the first non-conditional

			const defaultFlow: TStepFlowRenderedFlow|undefined = flows
					.find((f: TStepFlowRenderedFlow): boolean => f.value === null);
			if (!defaultFlow) throw new Error(`Step ${step.uid} has no field, but all its outflows have conditions`);

			return {
					uid: step.uid,
					type: step.type,
					flows: [ defaultFlow ],
					field: null
			};
		}
	
		const field: IInternalField|undefined = await this.fieldModel.getByIdOnly(step.field);
		if (!field) throw new Error('No associated field');
		
		return {
				uid: step.uid,
				type: step.type,
				flows: flows,
				field: field.uid
		};
	}

	public async render(triage: IInternalTriage): Promise<TStepFlowRenderedTriage|undefined> {
		const root: IInternalStep|undefined = await this.stepModel.getRootByParent(triage);
		if (!root) return undefined;

		const steps: IInternalStep[] = await this.stepModel.listByFirstClass(triage);
		const rendereds: TStepFlowRenderedStep[] = await Promise.all(
				steps
						.map((step: IInternalStep): Promise<TStepFlowRenderedStep> => this.renderStep(step))
		);

		return {
				root: root.uid,
				steps: rendereds
		};
	}
}
