import * as express from 'express';

import {
		commonsTypeAttemptBoolean,
		commonsTypeAttemptNumber,
		commonsTypeHasProperty,
		commonsTypeIsBoolean,
		commonsTypeIsNumber,
		commonsTypeIsString
} from 'tscommons-es-core';
import { ECommonsAdamantineFieldType } from 'tscommons-es-models-field';
import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiError,
		commonsRestApiNotFound,
		commonsRestApiBadRequest,
		commonsRestApiNotImplemented,
		commonsRestApiConflict
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { ICondition } from 'person-tracker-ts-assets';
import { TCondition, encodeICondition } from 'person-tracker-ts-assets';

import { StepModel, IInternalStep } from '../models/step.model';
import { FlowModel, IInternalFlow } from '../models/flow.model';
import { ConditionModel, IInternalCondition, decodeIInternalCondition } from '../models/condition.model';
import { TriageModel, IInternalTriage } from '../models/triage.model';
import { NamespaceModel, IInternalNamespace } from '../models/namespace.model';
import { FieldModel, IInternalField } from '../models/field.model';

export class ConditionApiHelper {
	constructor(
			private path: string,
			protected namespaceModel: NamespaceModel,
			protected triageModel: TriageModel,
			protected stepModel: StepModel,
			protected flowModel: FlowModel,
			protected conditionModel: ConditionModel,
			protected fieldModel: FieldModel
	) {}
	
	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		getHandler(
				`${this.path}data/:flow[id]/ids`,
				async (eReq: express.Request, _res: express.Response): Promise<number[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const flow: IInternalFlow|undefined = await this.flowModel.getByIdOnly(req.strictParams.flow as number);
					if (!flow) return commonsRestApiNotFound('No such flow exists');

					const conditions: IInternalCondition[] = await this.conditionModel.listByFirstClass(flow);
					
					// fudge
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(flow.step);
					if (!step) throw new Error('Foreign key mismatch. This should not be possible');

					if (step.type !== ECommonsStepFlowStepType.DATA) return commonsRestApiBadRequest('This step is not a DATA step, so conditions are not applicable');
					if (!step.field) return commonsRestApiError('Step is a data type but has not associate field. This should not be possible.');
						
					// fudge
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(step.triage);
					if (!triage) throw new Error('Foreign key mismatch. This should not be possible');
					
					// fudge
					const namespace: IInternalNamespace|undefined = await this.namespaceModel.getById(triage.namespace);
					if (!namespace) throw new Error('Foreign key namespace violation. This should not be possible');
					
					const field: IInternalField|undefined = await this.fieldModel.getByFirstClassAndId(namespace, step.field);
					if (
						field
						&& field.type === ECommonsAdamantineFieldType.ENUM
					) {
						if (field.options && field.options.length) {
							conditions
									.sort((a: IInternalCondition, b: IInternalCondition): number => {
										const aValue: unknown|undefined = a.value === undefined ? undefined : JSON.parse(a.value);
										const bValue: unknown|undefined = b.value === undefined ? undefined : JSON.parse(b.value);

										if (
											aValue !== undefined
											&& !commonsTypeIsString(aValue)
											&& !commonsTypeIsNumber(aValue)
											&& !commonsTypeIsBoolean(aValue)
										) throw new Error('Invalid JSON parse result');

										if (
											bValue !== undefined
											&& !commonsTypeIsString(bValue)
											&& !commonsTypeIsNumber(bValue)
											&& !commonsTypeIsBoolean(bValue)
										) throw new Error('Invalid JSON parse result');

										const aMatch: string|undefined = field.options!
												.find((option: string): boolean => option === aValue);
										const bMatch: string|undefined = field.options!
												.find((option: string): boolean => option === bValue);
										
										if (!aMatch && !bMatch) return 0;
										if (!aMatch && bMatch) return -1;
										if (aMatch && !bMatch) return 1;
										
										if (aMatch! < bMatch!) return -1;
										if (aMatch! > bMatch!) return 1;
										return 0;
									});
						}
					}
					
					return conditions
							.map((condition: IInternalCondition): number => condition.id);
				}
		);
		
		getHandler(
				`${this.path}data/:flow[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TCondition> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const flow: IInternalFlow|undefined = await this.flowModel.getByIdOnly(req.strictParams.flow as number);
					if (!flow) return commonsRestApiNotFound('No such flow exists');
					
					const condition: ICondition|undefined = await this.conditionModel.getByFlowAndId(flow, req.strictParams.id as number);
					if (!condition) return commonsRestApiNotFound('No such condition exists');
					
					return encodeICondition(condition);
				}
		);
	}
	
	public applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		postHandler(
				`${this.path}data/:flow[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TCondition> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const flow: IInternalFlow|undefined = await this.flowModel.getByIdOnly(req.strictParams.flow as number);
					if (!flow) return commonsRestApiNotFound('No such flow exists');
					
					if (!commonsTypeHasProperty(req.body, 'value')) return commonsRestApiBadRequest('No value specified');
					let value: unknown = req.body.value;
					if (
						!commonsTypeIsString(value)
						&& !commonsTypeIsNumber(value)
						&& !commonsTypeIsBoolean(value)
					) return commonsRestApiBadRequest('Valid is not a string, number or boolean');
					
					// fudge
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(flow.step);
					if (!step) throw new Error('Foreign key mismatch. This should not be possible');

					if (step.type !== ECommonsStepFlowStepType.DATA) return commonsRestApiBadRequest('This step is not a DATA step, so conditions are not applicable');
					if (!step.field) return commonsRestApiError('Step is a data type but has not associate field. This should not be possible.');
					
					// fudge
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(step.triage);
					if (!triage) throw new Error('Foreign key mismatch. This should not be possible');
					
					// fudge
					const namespace: IInternalNamespace|undefined = await this.namespaceModel.getById(triage.namespace);
					if (!namespace) throw new Error('Foreign key namespace violation. This should not be possible');

					const field: IInternalField|undefined = await this.fieldModel.getByFirstClassAndId(namespace, step.field);
					if (!field) throw new Error('No field set for this step. This should not be possible.');
					
					if (![ ECommonsAdamantineFieldType.ENUM, ECommonsAdamantineFieldType.BOOLEAN ].includes(field.type)) return commonsRestApiNotImplemented('Only ENUM and BOOLEAN field types are supported for step flow conditions');

					switch (field.type) {
						case ECommonsAdamantineFieldType.BOOLEAN: {
							value = commonsTypeAttemptBoolean(value) || false;
							break;
						}
						case ECommonsAdamantineFieldType.ENUM: {
							if (!commonsTypeIsString(value)) return commonsRestApiBadRequest('Value specified is not a string');
							if (field.multiple) {
								const valuesArray: string[] = value.split(',');
								for (const v of valuesArray) {
									if (!(field.options || []).includes(v)) return commonsRestApiBadRequest('Value is not in field enum options');
								}
								value = valuesArray.join(',');
							} else {
								if (!(field.options || []).includes(value)) return commonsRestApiBadRequest('Value is not in field enum options');
							}
							break;
						}
						case ECommonsAdamantineFieldType.INT:
						case ECommonsAdamantineFieldType.SMALLINT:
						case ECommonsAdamantineFieldType.TINYINT:
						case ECommonsAdamantineFieldType.FLOAT: {
							if (!commonsTypeIsString(value) && !commonsTypeIsNumber(value)) return commonsRestApiBadRequest('Value specified is not a number');
							value = commonsTypeAttemptNumber(value);
							if (value === undefined) return commonsRestApiBadRequest('Value specified is not a number');
							break;
						}
					}

					const existing: IInternalCondition|undefined = (await this.conditionModel.listByFirstClass(flow))
							.find((c: IInternalCondition): boolean => {
								if (c.value === undefined) return false;
								
								// needed to decode
								const d: ICondition = decodeIInternalCondition(c);
								
								switch (field.type) {
									case ECommonsAdamantineFieldType.BOOLEAN: {
										return d.value === value;
									}
									case ECommonsAdamantineFieldType.ENUM: {
										return d.value === value;
									}
									case ECommonsAdamantineFieldType.INT:
									case ECommonsAdamantineFieldType.SMALLINT:
									case ECommonsAdamantineFieldType.TINYINT:
									case ECommonsAdamantineFieldType.FLOAT: {
										return d.value === value;
									}
								}
								
								return false;
							});
					
					if (existing) return commonsRestApiConflict('A condition for this value already exists');
					
					const condition: IInternalCondition = await this.conditionModel.insertForFlow(
							flow,
							{
									value: value as string|number|boolean|undefined
							}
					);
					
					const decoded: ICondition = decodeIInternalCondition(condition);
					
					return encodeICondition(decoded);
				}
		);
		
		deleteHandler(
				`${this.path}data/:flow[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<boolean> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const flow: IInternalFlow|undefined = await this.flowModel.getByIdOnly(req.strictParams.flow as number);
					if (!flow) return commonsRestApiNotFound('No such flow exists');
					
					const condition: IInternalCondition|undefined = await this.conditionModel.getByFirstClassAndId(
							flow,
							req.strictParams.id as number
					);
					if (!condition) return commonsRestApiNotFound('No such condition exists');
					
					await this.conditionModel.deleteForFirstClass(
							flow,
							condition
					);
					
					return true;
				}
		);
	}
}
