import * as express from 'express';

import { TEncodedObject } from 'tscommons-es-core';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiError,
		commonsRestApiNotFound
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { TStep, encodeIStep } from 'person-tracker-ts-assets';

import { TriageModel, IInternalTriage } from '../models/triage.model';
import { StepModel, IInternalStep } from '../models/step.model';
import { FieldModel, IInternalField } from '../models/field.model';
import { NamespaceModel, IInternalNamespace } from '../models/namespace.model';

export class StepApiHelper {
	constructor(
			private path: string,
			protected namespaceModel: NamespaceModel,
			protected triageModel: TriageModel,
			protected stepModel: StepModel,
			protected fieldModel: FieldModel
	) {}
	
	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		getHandler(
				`${this.path}data/:triage[id]/root-step`,
				async (eReq: express.Request, _res: express.Response): Promise<TStep & TEncodedObject> => {	// hacky way of avoiding the optional fields
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(
							req.strictParams.triage as number
					);
					if (!triage) return commonsRestApiNotFound('No such triage exists');
					
					const step: IInternalStep|undefined = await this.stepModel.getRootByParent(triage);
					if (!step) return commonsRestApiError('No root step for this triage. This should not be possible');
					
					return encodeIStep(step) as TStep & TEncodedObject;
				}
		);
		
		getHandler(
				`${this.path}data/:triage[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TStep & TEncodedObject> => {	// hacky way of avoiding the optional fields
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(
							req.strictParams.triage as number
					);
					if (!triage) return commonsRestApiNotFound('No such triage exists');
					
					const step: IInternalStep|undefined = await this.stepModel.getByFirstClassAndId(
							triage,
							req.strictParams.id as number
					);
					if (!step) return commonsRestApiError('No such step exists');
					
					return encodeIStep(step) as TStep & TEncodedObject;
				}
		);
		
		getHandler(
				`${this.path}data/uids/:uid[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<TStep & TEncodedObject> => {	// hacky way of avoiding the optional fields
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const step: IInternalStep|undefined = await this.stepModel.getByUid(req.strictParams.uid as string);
					if (!step) return commonsRestApiError('No such step exists');
					
					return encodeIStep(step) as TStep & TEncodedObject;
				}
		);
	}
	
	public applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		postHandler(
				`${this.path}data/:triage[id]/field/:field[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TStep & TEncodedObject> => {	// hacky way of avoiding the optional fields
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(
							req.strictParams.triage as number
					);
					if (!triage) return commonsRestApiNotFound('No such triage exists');
					
					// fudge
					const namespace: IInternalNamespace|undefined = await this.namespaceModel.getById(triage.namespace);
					if (!namespace) throw new Error('Foreign key namespace violation. This should not be possible');
					
					const field: IInternalField|undefined = await this.fieldModel.getByFirstClassAndId(namespace, req.strictParams.field as number);
					if (!field) return commonsRestApiNotFound('No such field exists');
					
					const step: IInternalStep = await this.stepModel.createField(
							triage,
							field
					);
					
					return encodeIStep(step) as TStep & TEncodedObject;
				}
		);
		postHandler(
				`${this.path}data/:triage[id]/complete`,
				async (eReq: express.Request, _res: express.Response): Promise<TStep & TEncodedObject> => {	// hacky way of avoiding the optional fields
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(
							req.strictParams.triage as number
					);
					if (!triage) return commonsRestApiNotFound('No such triage exists');
					
					const step: IInternalStep = await this.stepModel.createComplete(triage);
					
					return encodeIStep(step) as TStep & TEncodedObject;
				}
		);
		postHandler(
				`${this.path}data/:triage[id]/abort`,
				async (eReq: express.Request, _res: express.Response): Promise<TStep & TEncodedObject> => {	// hacky way of avoiding the optional fields
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(
							req.strictParams.triage as number
					);
					if (!triage) return commonsRestApiNotFound('No such triage exists');
					
					const step: IInternalStep = await this.stepModel.createAbort(triage);
					
					return encodeIStep(step) as TStep & TEncodedObject;
				}
		);

		patchHandler(
				`${this.path}data/:triage[id]/ids/:step[id]/:field[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TStep & TEncodedObject> => {	// hacky way of avoiding the optional fields
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(
							req.strictParams.triage as number
					);
					if (!triage) return commonsRestApiNotFound('No such triage exists');

					const step: IInternalStep|undefined = await this.stepModel.getByFirstClassAndId(
							triage,
							req.strictParams.step as number
					);
					if (!step) return commonsRestApiNotFound('No such step exists');

					// fudge
					const namespace: IInternalNamespace|undefined = await this.namespaceModel.getById(triage.namespace);
					if (!namespace) throw new Error('Foreign key namespace violation. This should not be possible');
					
					const field: IInternalField|undefined = await this.fieldModel.getByFirstClassAndId(namespace, req.strictParams.field as number);
					if (!field) return commonsRestApiNotFound('No such field exists');

					step.field = field.id;
					
					await this.stepModel.updateForFirstClass(triage, step);

					const result: IInternalStep|undefined = await this.stepModel.getByFirstClassAndId(triage, step.id);
					if (!result) return commonsRestApiError('Updated step no longer exists');
					
					return encodeIStep(result) as TStep & TEncodedObject;
				}
		);
		
		deleteHandler(
				`${this.path}data/:triage[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<boolean> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const triage: IInternalTriage|undefined = await this.triageModel.getByIdOnly(
							req.strictParams.triage as number
					);
					if (!triage) return commonsRestApiNotFound('No such triage exists');

					const existing: IInternalStep|undefined = await this.stepModel.getByFirstClassAndId(
							triage,
							req.strictParams.id as number
					);
					if (!existing) return commonsRestApiNotFound('No such step exists');
					
					await this.stepModel.deleteForFirstClass(triage, existing);
					
					return true;
				}
		);
	}
}
