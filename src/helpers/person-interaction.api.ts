import * as express from 'express';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiNotFound,
		commonsRestApiConflict
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { PersonModel } from '../models/person.model';
import { InteractionModel } from '../models/interaction.model';
import { PersonInteractionModel } from '../models/person-interaction.model';
import { IInternalPerson } from '../models/person.model';
import { IInternalInteraction } from '../models/interaction.model';

// Can't use ManagedM2MLinkTableApi as IPerson is not Managed

export class PersonInteractionApiHelper {
	constructor(
			private path: string,
			protected personModel: PersonModel,
			protected interactionModel: InteractionModel,
			protected personInteractionModel: PersonInteractionModel
	) {}

	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		// list Bs by Person
		getHandler(
				`${this.path}persons/:person[id]/interactions`,
				async (eReq: express.Request, _res: express.Response): Promise<number[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(req.strictParams.person as number);
					if (!person) return commonsRestApiNotFound('No such person exists');
					
					const bs: IInternalInteraction[] = await this.personInteractionModel.listBsByA(person);
					
					return bs
							.map((interaction: IInternalInteraction): number => interaction.id);
				}
		);

		// list As by Interaction
		getHandler(
				`${this.path}interactions/:interaction[id]/persons`,
				async (eReq: express.Request, _res: express.Response): Promise<number[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');
					
					const persons: IInternalPerson[] = await this.personInteractionModel.listAsByB(interaction);
					
					return persons
							.map((person: IInternalPerson): number => person.id);
				}
		);
	}
	
	public applyWriteData(
			_postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postLinkCallback?: (
					person: IInternalPerson,
					interaction: IInternalInteraction,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postUnlinkCallback?: (
					person: IInternalPerson,
					interaction: IInternalInteraction,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): void {
		// link
		putHandler(
				`${this.path}persons/:person[id]/interactions/:interaction[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<true> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(req.strictParams.person as number);
					if (!person) return commonsRestApiNotFound('No such person exists');

					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');
					
					if (await this.personInteractionModel.isLinked(person, interaction)) return commonsRestApiConflict('Link row for model items already exists');

					await this.personInteractionModel.link(person, interaction);
					
					if (postLinkCallback) {
						await postLinkCallback(
								person,
								interaction,
								req
						);
					}

					return true;
				}
		);

		// unlink
		deleteHandler(
				`${this.path}persons/:person[id]/interactions/:interaction[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<true> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(req.strictParams.person as number);
					if (!person) return commonsRestApiNotFound('No such person exists');

					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');
					
					if (!(await this.personInteractionModel.isLinked(person, interaction))) return commonsRestApiNotFound('Model items are not linked');

					await this.personInteractionModel.unlink(person, interaction);
					
					if (postUnlinkCallback) {
						await postUnlinkCallback(
								person,
								interaction,
								req
						);
					}

					return true;
				}
		);
	}
}
