import * as express from 'express';

import { commonsTypeAttemptBoolean, commonsTypeHasProperty, commonsTypeHasPropertyString, commonsTypeHasPropertyStringArray } from 'tscommons-es-core';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiBadRequest,
		commonsRestApiConflict,
		commonsRestApiError,
		commonsRestApiNotFound
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { commonsOutputDebug } from 'nodecommons-es-cli';

import { IInteraction, IInteractionData, IPersonData, IInteractionNote, IPersonNote, IPerson, IData, IField, INote } from 'person-tracker-ts-assets';

import { EventModel, IInternalEvent } from '../models/event.model';
import { InteractionModel, IInternalInteraction } from '../models/interaction.model';
import { DataModel } from '../models/data.model';
import { NoteModel } from '../models/note.model';
import { PersonInteractionModel } from '../models/person-interaction.model';
import { IInternalPerson, PersonModel } from '../models/person.model';
import { FieldModel } from '../models/field.model';

export class EngineerApiHelper<
		DataT extends IData = IData,
		NoteT extends INote = INote
> {
	constructor(
			private path: string,
			protected eventModel: EventModel,
			protected fieldModel: FieldModel,
			protected interactionModel: InteractionModel,
			protected personInteractionModel: PersonInteractionModel,
			protected personModel: PersonModel,
			protected dataModel: DataModel<DataT>,
			protected notesModel: NoteModel<NoteT>
	) {}
	
	public applyWriteData(
			_postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postMergeInteractionCallback?: (
					event: IInternalEvent,
					mergedInteraction: IInternalInteraction,
					removedInteractions: IInternalInteraction[],
					mergedPerson: IInternalPerson|undefined,
					removedPersons: IInternalPerson[]|undefined,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): void {
		patchHandler(
				`${this.path}interaction/merge/:event[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<string> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const uids: string[] = [];
					if (commonsTypeHasPropertyStringArray(eReq.body, 'uids')) uids.push(...eReq.body.uids as string[]);
					if (commonsTypeHasPropertyString(eReq.body, 'uids')) uids.push(...(eReq.body.uids as string).split(','));

					if (uids.length < 2) return commonsRestApiBadRequest('A minimum of 2 UIDs must be supplied to merge');

					const mergePersonData: boolean = commonsTypeHasProperty(eReq.body, 'personData') ? (commonsTypeAttemptBoolean(eReq.body['personData']) || false) : false;

					const interactions: IInteraction[] = [];
					
					for (const uid of uids) {
						const interaction: IInteraction|undefined = await this.interactionModel.getByUid(uid);
						if (!interaction) return commonsRestApiNotFound('Missing interaction');

						if (interaction.event !== event.id) return commonsRestApiConflict('Event mismatch for interactions');

						interactions.push(interaction);
					}
					if (interactions.length !== uids.length) return commonsRestApiError('UID/interactions length mismatch. This should not be possible. Stopping for safety.');

					commonsOutputDebug(`Performing interaction merge for ${uids.join(',')}`);

					const mergedInteraction: IInteraction = await this.interactionModel.createForEvent(event);
					commonsOutputDebug(`Destination interaction UID is ${mergedInteraction.uid}`);

					let mergedPerson: IInternalPerson|undefined;
					const originalPersons: IPerson[] = [];

					for (const interaction of interactions) {
						{	// scope
							commonsOutputDebug(`Merging interaction ${interaction.uid} data into destination ${mergedInteraction.uid}`);

							const datas: IInteractionData<DataT>[] = await this.dataModel.listByInteraction(event, interaction);
							for (const data of datas) {
								const field: IField|undefined = await this.fieldModel.getByIdOnly(data.field);
								if (!field) throw new Error('Missing field. Foreign key violation');

								data.interaction = mergedInteraction.id;
								await this.dataModel.updateLogEntry(event, field, data);
							}
						}

						{	// scope
							commonsOutputDebug(`Merging interaction ${interaction.uid} notes into destination ${mergedInteraction.uid}`);

							const notes: IInteractionNote<NoteT>[] = await this.notesModel.listByInteraction(event, interaction);
							for (const note of notes) {
								note.interaction = mergedInteraction.id;
								await this.notesModel.updateForFirstClass(event, note);
							}
						}

						const persons: IPerson[] = await this.personInteractionModel.listAsByB(interaction);
						originalPersons.push(...persons);

						if (mergePersonData && persons.length > 0) {
							if (!mergedPerson) {
								commonsOutputDebug('Creating new destination person');

								mergedPerson = await this.personModel.createForEvent(event);
								await this.personInteractionModel.link(mergedPerson, mergedInteraction);

								commonsOutputDebug(`New uid is ${mergedPerson.uid}`);
							}

							for (const person of persons) {
								commonsOutputDebug('Merging person data');

								const datas: IPersonData<DataT>[] = await this.dataModel.listByPerson(event, person);
								for (const data of datas) {
									const field: IField|undefined = await this.fieldModel.getByIdOnly(data.field);
									if (!field) throw new Error('Missing field. Foreign key violation');

									data.person = mergedPerson.id;
									await this.dataModel.updateLogEntry(event, field, data);
								}

								commonsOutputDebug('Merging person notes');

								const notes: IPersonNote<NoteT>[] = await this.notesModel.listByPerson(event, person);
								for (const note of notes) {
									note.person = mergedPerson.id;
									await this.notesModel.updateForFirstClass(event, note);
								}
							}
						}
					}

					commonsOutputDebug('Successfully merged');

					commonsOutputDebug('Cleaining up old interactions and persons');

					for (const interaction of interactions) {
						await this.interactionModel.deleteForFirstClass(event, interaction);
					}
					for (const person of originalPersons) {
						await this.personModel.deleteForFirstClass(event, person);
					}

					commonsOutputDebug('Successfully cleaned');

					if (postMergeInteractionCallback) {
						await postMergeInteractionCallback(
								event,
								mergedInteraction,
								interactions,
								mergedPerson,
								originalPersons,
								req
						);
					}

					return mergedInteraction.uid;
				}
		);
	}
}
