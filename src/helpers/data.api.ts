import * as express from 'express';

import {
		commonsDateHisToDate,
		commonsDateYmdHisToDate,
		commonsDateYmdToDate,
		commonsTypeAttemptBoolean,
		commonsTypeHasProperty,
		commonsTypeHasPropertyNumber,
		commonsTypeHasPropertyString,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		COMMONS_REGEX_PATTERN_DATE_YMD,
		COMMONS_REGEX_PATTERN_TIME_HIS,
		TEncodedObject
} from 'tscommons-es-core';
import { ECommonsAdamantineFieldType } from 'tscommons-es-models-field';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiNotFound,
		commonsRestApiBadRequest,
		commonsRestApiNotImplemented,
		commonsRestApiConflict,
		commonsRestApiError
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { encodeIData as defaultEncodeIData, IData, IEvent, IField, IInteraction, IPerson, TData } from 'person-tracker-ts-assets';

import { PersonModel } from '../models/person.model';
import { InteractionModel } from '../models/interaction.model';
import { IInternalPerson } from '../models/person.model';
import { IInternalInteraction } from '../models/interaction.model';
import { DataModel } from '../models/data.model';
import { EventModel, IInternalEvent } from '../models/event.model';
import { FieldModel, IInternalField } from '../models/field.model';

function encodeValue(
		field: IField,
		body: { [ key: string ]: unknown }
): string|number|boolean|Date|undefined {
	switch (field.type) {
		case ECommonsAdamantineFieldType.DATE: {
			if (!commonsTypeHasPropertyString(body, 'value')) return commonsRestApiBadRequest('No value in the body');
			if (!COMMONS_REGEX_PATTERN_DATE_YMD.test(body['value'] as string)) return commonsRestApiBadRequest('Invalid date');
			return commonsDateYmdToDate(body['value'] as string, true);
		}
		case ECommonsAdamantineFieldType.TIME: {
			if (!commonsTypeHasPropertyString(body, 'value')) return commonsRestApiBadRequest('No value in the body');
			if (!COMMONS_REGEX_PATTERN_TIME_HIS.test(body['value'] as string)) return commonsRestApiBadRequest('Invalid time');
			return commonsDateHisToDate(body['value'] as string, true);
		}
		case ECommonsAdamantineFieldType.DATETIME: {
			if (!commonsTypeHasPropertyString(body, 'value')) return commonsRestApiBadRequest('No value in the body');
			if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(body['value'] as string)) return commonsRestApiBadRequest('Invalid datetime');
			return commonsDateYmdHisToDate(body['value'] as string, true);
		}
		case ECommonsAdamantineFieldType.ENUM: {
			if (!commonsTypeHasPropertyString(body, 'value')) return commonsRestApiBadRequest('No value in the body');
			if (!(field.options || []).includes(body['value'] as string)) return commonsRestApiBadRequest('Invalid enum value');
			return body['value'] as string;
		}
		case ECommonsAdamantineFieldType.STRING: {
			if (!commonsTypeHasPropertyString(body, 'value')) return commonsRestApiBadRequest('No value in the body');
			return body['value'] as string;
		}
		case ECommonsAdamantineFieldType.BOOLEAN: {
			return commonsTypeAttemptBoolean(body['value']);
		}
		case ECommonsAdamantineFieldType.INT:
		case ECommonsAdamantineFieldType.SMALLINT:
		case ECommonsAdamantineFieldType.TINYINT:
		case ECommonsAdamantineFieldType.FLOAT: {
			if (!commonsTypeHasPropertyNumber(body, 'value')) return commonsRestApiBadRequest('No number value in the body');
			return body['value'] as number;
		}
	}

	return undefined;
}

export class DataApiHelper<
		DataI extends IData = IData,
		DataT extends TData = TData
> {
	constructor(
			private path: string,
			protected eventModel: EventModel,
			protected personModel: PersonModel,
			protected interactionModel: InteractionModel,
			protected fieldModel: FieldModel,
			protected dataModel: DataModel<DataI>,
			private insertCallback: (
					event: IInternalEvent,
					timestamp: Date,
					field: IInternalField,
					value: number|string|boolean|Date|undefined,
					person?: IInternalPerson,
					interaction?: IInternalInteraction,
					additionalValues?: { [ key: string ]: unknown }
			) => Promise<DataI>,
			private deletedCallback: (data: DataI) => Promise<void>,
			private encodeIData: (data: DataI) => DataT = defaultEncodeIData,
			private getAdditionalValues?: (
					req: ICommonsRequestWithStrictParams,
					event: IEvent,
					interaction: IInteraction|undefined,
					person: IPerson|undefined,
					field: IField
			) => Promise<{ [ key: string ]: unknown }>
	) {}

	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		// list all for event
		getHandler(
				`${this.path}data/:event[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<DataT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const datas: DataI[] = await this.dataModel.listByEvent(event);
					
					return datas
							.map((data: DataI): DataT => this.encodeIData(data));
				}
		);

		// list all for event since logno
		getHandler(
				`${this.path}data/:event[id]/since/:logno[int]`,
				async (eReq: express.Request, _res: express.Response): Promise<DataT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const datas: DataI[] = await this.dataModel.listByEventSinceLogno(
							event,
							req.strictParams.logno as number
					);
					
					return datas
							.map((data: DataI): DataT => this.encodeIData(data));
				}
		);

		// list by Interaction
		getHandler(
				`${this.path}data/:event[id]/interactions/:interaction[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<DataT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');

					const datas: DataI[] = await this.dataModel.listByInteraction(event, interaction) as DataI[];
					
					return datas
							.map((data: DataI): DataT => this.encodeIData(data));
				}
		);

		// list by Person
		getHandler(
				`${this.path}data/:event[id]/persons/:person[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<DataT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(req.strictParams.person as number);
					if (!person) return commonsRestApiNotFound('No such person exists');

					const datas: DataI[] = await this.dataModel.listByPerson(event, person);
					
					return datas
							.map((data: DataI): DataT => this.encodeIData(data));
				}
		);
	}

	private async insertData(
			req: ICommonsRequestWithStrictParams,
			interaction: IInternalInteraction|undefined,
			person: IInternalPerson|undefined,
			postNewDataCallback?: (
					event: IInternalEvent,
					context: IInternalInteraction|IInternalPerson,
					field: IField,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): Promise<DataI> {
		const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
		if (!event) return commonsRestApiNotFound('No such event exists');

		const field: IInternalField|undefined = await this.fieldModel.getByIdOnly(req.strictParams.field as number);
		if (!field) return commonsRestApiNotFound('No such field exists');

		const body: TEncodedObject = req.body as TEncodedObject;
		if (!commonsTypeHasProperty(body, 'value')) return commonsRestApiBadRequest('No body value supplied');
		
		// timestamp is client timestamp, not server insert timestamp
		if (!commonsTypeHasPropertyString(body, 'timestamp')) return commonsRestApiBadRequest('No timestamp value supplied');
		if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(body['timestamp'] as string)) return commonsRestApiBadRequest('Invalid timestamp value supplied');
		const timestamp: Date = commonsDateYmdHisToDate(body['timestamp'] as string, true);

		const value: string|number|boolean|Date|undefined = encodeValue(field, body);
		if (value === undefined) return commonsRestApiNotImplemented('Field type is not implemented');

		let additionalValues: { [ key: string ]: unknown }|undefined;
		if (this.getAdditionalValues) {
			additionalValues = await this.getAdditionalValues(
					req,
					event,
					interaction,
					person,
					field
			);
		}

		const data: DataI = await this.insertCallback(
				event,
				timestamp,
				field,
				value,
				person,
				interaction,
				additionalValues
		);

		if (interaction && postNewDataCallback) await postNewDataCallback(event, interaction, field, req);
		if (person && postNewDataCallback) await postNewDataCallback(event, person, field, req);

		return data;
	}
	
	public applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postNewInteractionDataCallback?: (
					event: IInternalEvent,
					interaction: IInternalInteraction,
					field: IField,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postNewPersonDataCallback?: (
					event: IInternalEvent,
					person: IInternalPerson,
					field: IField,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postUpdateDataCallback?: (
					event: IInternalEvent,
					data: DataI,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postDeleteDataCallback?: (
					event: IInternalEvent,
					data: DataI,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): void {
		// add interaction data
		postHandler(
				`${this.path}data/:event[id]/interactions/:interaction[id]/fields/:field[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<DataT> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');

					const data: DataI = await this.insertData(
							req,
							interaction,
							undefined,
							async (
									event: IEvent,
									context: IInteraction|IPerson,
									field: IField,
									req2: ICommonsRequestWithStrictParams
							): Promise<void> => {
								if (postNewInteractionDataCallback) {
									await postNewInteractionDataCallback(
											event,
											context,
											field,
											req2
									);
								}
							}
					);

					return this.encodeIData(data);
				}
		);

		// add person data
		// event is needed for Data even though it isn't for Person
		postHandler(
				`${this.path}data/:event[id]/persons/:person[id]/fields/:field[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<DataT> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(req.strictParams.person as number);
					if (!person) return commonsRestApiNotFound('No such person exists');

					const data: DataI = await this.insertData(
							req,
							undefined,
							person,
							async (
									event: IEvent,
									context: IInteraction|IPerson,
									field: IField,
									req2: ICommonsRequestWithStrictParams
							): Promise<void> => {
								if (postNewPersonDataCallback) {
									await postNewPersonDataCallback(
											event,
											context,
											field,
											req2
									);
								}
							}
					);

					return this.encodeIData(data);
				}
		);

		// edit interaction or person data
		patchHandler(
				`${this.path}data/:event[id]/uids/:uid[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<true> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');
			
					const existing: DataI|undefined = await this.dataModel.getByUid(req.strictParams.uid as string);
					if (!existing) return commonsRestApiNotFound('No such data exists');
					if (existing.event !== event.id) return commonsRestApiConflict('Data vs event mismatch');

					// We can't check for conflict, as there isn't an interaction/person parameter sent

					// if (isIInteractionData(existing)) {
					// 	const interaction: IInteraction|undefined = await this.interactionModel.getByFirstClassAndId(event, existing.interaction);
					// 	if (!interaction) return commonsRestApiConflict('Data vs interaction ID mismatch');
			
					// 	if (interaction.id !== req.strictParams.interaction) return commonsRestApiConflict('Interaction mismatch');
					// 	if (interaction.event !== event.id) return commonsRestApiConflict('Interaction mismatch');
					// } else if (isIPersonData(existing)) {
					// 	const person: IPerson|undefined = await this.personModel.getByFirstClassAndId(event, existing.person);
					// 	if (!person) return commonsRestApiConflict('Data vs person ID mismatch');
			
					// 	if (person.id !== req.strictParams.person) return commonsRestApiConflict('Person mismatch');
					// 	if (person.event !== event.id) return commonsRestApiConflict('Person mismatch');
					// } else {
					// 	return commonsRestApiNotImplemented('Data is neither interaction or person');
					// }
			
					const body: TEncodedObject = req.body as TEncodedObject;
					if (!commonsTypeHasProperty(body, 'value')) return commonsRestApiBadRequest('No body value supplied');

					const field: IField|undefined = await this.fieldModel.getByIdOnly(existing.field);
					if (!field) return commonsRestApiError('No field for that value. This is a foreign key violation');

					const value: string|number|boolean|Date|undefined = encodeValue(field, body);
					if (value === undefined) return commonsRestApiNotImplemented('Field type is not implemented');
								
					await this.dataModel.updateLogEntry(
							event,
							field,
							{
									...existing,
									value: value
							}
					);

					if (postUpdateDataCallback) {
						await postUpdateDataCallback(
								event,
								existing,	// this is technically the previous value, but we only use it for UID/ID etc., so it doesn't matter
								req
						);
					}
			
					return true;
				}
		);

		// delete data
		deleteHandler(
				`${this.path}data/:event[id]/uids/:uid[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<boolean> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const data: DataI|undefined = await this.dataModel.getByUid(req.strictParams['uid'] as string);
					if (!data) return commonsRestApiNotFound('No such data exists');

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');
					if (data.event !== event.id) return commonsRestApiConflict('Data vs event mismatch');
			
					await this.dataModel.deleteForFirstClass(
							event,
							// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
							data as any	// not ideal, but the only problem is the value, which isn't used to delete anyway
					);
					
					await this.deletedCallback(data);
					
					if (postDeleteDataCallback) {
						await postDeleteDataCallback(
								event,
								data,
								req
						);
					}

					return true;
				}
		);
	}
}
