import * as express from 'express';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiError,
		commonsRestApiNotFound
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { TPerson, encodeIPerson } from 'person-tracker-ts-assets';

import { PersonModel, IInternalPerson } from '../models/person.model';
import { EventModel, IInternalEvent } from '../models/event.model';

export class PersonApiHelper {
	constructor(
			private path: string,
			protected eventModel: EventModel,
			protected personModel: PersonModel
	) {}
	
	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		getHandler(
				`${this.path}data/:event[id]/ids`,
				async (eReq: express.Request, _res: express.Response): Promise<number[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const persons: IInternalPerson[] = await this.personModel.listByFirstClass(event);

					return persons
							.map((person: IInternalPerson): number => person.id);
				}
		);
		
		getHandler(
				`${this.path}data/:event[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TPerson[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const persons: IInternalPerson[] = await this.personModel.listByFirstClass(event);
				
					return persons
							.map((person: IInternalPerson): TPerson => encodeIPerson(person));
				}
		);
		
		getHandler(
				`${this.path}data/:event[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TPerson> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');
				
					const person: IInternalPerson|undefined = await this.personModel.getByFirstClassAndId(
							event,
							req.strictParams.id as number
					);
					if (!person) return commonsRestApiError('No such person exists');
					
					return encodeIPerson(person);
				}
		);
		
		getHandler(
				`${this.path}data/uids/:uid[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<TPerson> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const person: IInternalPerson|undefined = await this.personModel.getByUid(req.strictParams.uid as string);
					if (!person) return commonsRestApiError('No such person exists');
					
					return encodeIPerson(person);
				}
		);
	}
	
	public applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postNewCallback?: (
					event: IInternalEvent,
					person: IInternalPerson,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postDeleteCallback?: (
					event: IInternalEvent,
					person: IInternalPerson,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): void {
		postHandler(
				`${this.path}data/:event[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TPerson> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');
					
					const person: IInternalPerson = await this.personModel.createForEvent(event);
					
					if (postNewCallback) {
						await postNewCallback(
								event,
								person,
								req
						);
					}

					return encodeIPerson(person);
				}
		);
		
		deleteHandler(
				`${this.path}data/:event[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<boolean> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const existing: IInternalPerson|undefined = await this.personModel.getByFirstClassAndId(
							event,
							req.strictParams.id as number
					);
					if (!existing) return commonsRestApiNotFound('No such person exists');
					
					await this.personModel.deleteForFirstClass(event, existing);

					if (postDeleteCallback) {
						await postDeleteCallback(
								event,
								existing,
								req
						);
					}

					return true;
				}
		);
	}
}
