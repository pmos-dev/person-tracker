import * as express from 'express';

import { commonsTypeHasPropertyString } from 'tscommons-es-core';
import { commonsDateYmdToDate } from 'tscommons-es-core';
import { TDateRange } from 'tscommons-es-core';
import { COMMONS_REGEX_PATTERN_DATE_YMD } from 'tscommons-es-core';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiNotFound,
		commonsRestApiBadRequest
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ManagedSecondClassApi } from 'nodecommons-es-rest-adamantine';

import { TEvent, encodeIEvent } from 'person-tracker-ts-assets';

import { NamespaceModel, IInternalNamespace } from '../models/namespace.model';
import { EventModel, IInternalEvent } from '../models/event.model';

export class EventApiHelper {
	constructor(
			private path: string,
			protected namespaceModel: NamespaceModel,
			protected eventModel: EventModel
	) {}
	
	public applyData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		const managedApi: ManagedSecondClassApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalEvent,
				IInternalNamespace,
				EventModel,
				TEvent
		> = new ManagedSecondClassApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalEvent,
				IInternalNamespace,
				EventModel,
				TEvent
		>(
				this.path,
				this.eventModel,
				encodeIEvent
		);
		
		managedApi.apply(
				// getHandler
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => getHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => postHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => putHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => patchHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => deleteHandler(
						query,
						handler
				)
		);
		
		getHandler(
				`${this.path}data/:namespace[id]/date-range`,
				async (eReq: express.Request, _res: express.Response): Promise<TEvent[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const namespace: IInternalNamespace|undefined = await this.namespaceModel.getById(
							req.strictParams.namespace as number
					);
					if (!namespace) return commonsRestApiNotFound('No such namespace exists');
					
					if (!commonsTypeHasPropertyString(req.query, 'from')) return commonsRestApiBadRequest('No date range from supplied');
					if (!COMMONS_REGEX_PATTERN_DATE_YMD.test(req.query.from as string)) return commonsRestApiBadRequest('Invalid date range from supplied');
					
					if (!commonsTypeHasPropertyString(req.query, 'to')) return commonsRestApiBadRequest('No date range to supplied');
					if (!COMMONS_REGEX_PATTERN_DATE_YMD.test(req.query.to as string)) return commonsRestApiBadRequest('Invalid date range to supplied');
					
					const dateRange: TDateRange = {
							from: commonsDateYmdToDate(req.query.from as string, true),
							to: commonsDateYmdToDate(req.query.to as string, true)
					};
					
					const events: IInternalEvent[] = await this.eventModel.listByNamespaceAndDateRange(
							namespace,
							dateRange
					);
					
					return events
							.map((event: IInternalEvent): TEvent => encodeIEvent(event));
				}
		);
	}
}
