import * as express from 'express';

import {
		commonsDateYmdHisToDate,
		commonsTypeHasProperty,
		commonsTypeHasPropertyString,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		TEncodedObject
} from 'tscommons-es-core';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiNotFound,
		commonsRestApiBadRequest,
		commonsRestApiConflict,
		commonsRestApiNotImplemented,
		TCommonsContentHandler
} from 'nodecommons-es-rest';

import {
		encodeINote as defaultEncodeINote,
		IEvent,
		IInteraction,
		IInteractionNote,
		INote,
		IPerson,
		IPersonNote,
		isIInteractionNote,
		isIPersonNote,
		TNote
} from 'person-tracker-ts-assets';

import { PersonModel } from '../models/person.model';
import { InteractionModel } from '../models/interaction.model';
import { IInternalPerson } from '../models/person.model';
import { IInternalInteraction } from '../models/interaction.model';
import { NoteModel } from '../models/note.model';
import { EventModel, IInternalEvent } from '../models/event.model';

export class NoteApiHelper<
		NoteI extends INote = INote,
		NoteT extends TNote = TNote
> {
	constructor(
			private path: string,
			protected eventModel: EventModel,
			protected personModel: PersonModel,
			protected interactionModel: InteractionModel,
			protected noteModel: NoteModel<NoteI>,
			private insertCallback: (
					event: IInternalEvent,
					timestamp: Date,
					value: string,
					person?: IInternalPerson,
					interaction?: IInternalInteraction,
					additionalValues?: { [ key: string ]: unknown }
			) => Promise<NoteI>,
			private deletedCallback: (note: NoteI) => void,
			private encodeINote: (note: NoteI) => NoteT = defaultEncodeINote,
			private getAdditionalValues?: (
					req: ICommonsRequestWithStrictParams,
					event: IEvent,
					interaction: IInteraction|undefined,
					person: IPerson|undefined
			) => Promise<{ [ key: string ]: unknown }>
	) {}

	public applyReadNote(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		// list all for event
		getHandler(
				`${this.path}data/:event[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<NoteT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const notes: NoteI[] = await this.noteModel.listByEvent(event);
					
					return notes
							.map((note: NoteI): NoteT => this.encodeINote(note));
				}
		);

		// list all for event since logno
		getHandler(
				`${this.path}data/:event[id]/since/:logno[int]`,
				async (eReq: express.Request, _res: express.Response): Promise<NoteT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const notes: NoteI[] = await this.noteModel.listByEventSinceLogno(
							event,
							req.strictParams.logno as number
					);
					
					return notes
							.map((note: NoteI): NoteT => this.encodeINote(note));
				}
		);

		// list by Interaction
		getHandler(
				`${this.path}data/:event[id]/interactions/:interaction[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<NoteT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');

					const notes: IInteractionNote<NoteI>[] = await this.noteModel.listByInteraction(event, interaction);
					
					return notes
							.map((note: NoteI): NoteT => this.encodeINote(note));
				}
		);

		// list by Person
		getHandler(
				`${this.path}data/:event[id]/persons/:person[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<NoteT[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(req.strictParams.person as number);
					if (!person) return commonsRestApiNotFound('No such person exists');

					const notes: IPersonNote<NoteI>[] = await this.noteModel.listByPerson(event, person);
					
					return notes
							.map((note: NoteI): NoteT => this.encodeINote(note));
				}
		);
	}

	private async insertNote(
			req: ICommonsRequestWithStrictParams,
			interaction: IInternalInteraction|undefined,
			person: IInternalPerson|undefined,
			postNewNoteCallback?: (
					event: IInternalEvent,
					context: IInternalInteraction|IInternalPerson,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): Promise<NoteI> {
		const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
		if (!event) return commonsRestApiNotFound('No such event exists');

		const body: TEncodedObject = req.body as TEncodedObject;
		if (!commonsTypeHasProperty(body, 'value')) return commonsRestApiBadRequest('No body value supplied');
		
		// timestamp is client timestamp, not server insert timestamp
		if (!commonsTypeHasPropertyString(body, 'timestamp')) return commonsRestApiBadRequest('No timestamp value supplied');
		if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(body['timestamp'] as string)) return commonsRestApiBadRequest('Invalid timestamp value supplied');
		const timestamp: Date = commonsDateYmdHisToDate(body['timestamp'] as string, true);

		if (!commonsTypeHasPropertyString(body, 'value')) return commonsRestApiBadRequest('No value in the body');

		let additionalValues: { [ key: string ]: unknown }|undefined;
		if (this.getAdditionalValues) {
			additionalValues = await this.getAdditionalValues(
					req,
					event,
					interaction,
					person
			);
		}

		const note: NoteI = await this.insertCallback(
				event,
				timestamp,
				body['value'] as string,
				person,
				interaction,
				additionalValues
		);

		if (interaction && postNewNoteCallback) await postNewNoteCallback(event, interaction, req);
		if (person && postNewNoteCallback) await postNewNoteCallback(event, person, req);

		return note;
	}

	public applyWriteNote(
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postNewInteractionNoteCallback?: (
					event: IInternalEvent,
					interaction: IInternalInteraction,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postNewPersonNoteCallback?: (
					event: IInternalEvent,
					person: IInternalPerson,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postUpdateNoteCallback?: (
					event: IInternalEvent,
					note: NoteI,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postDeleteNoteCallback?: (
					event: IInternalEvent,
					note: NoteI,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): void {
		// add interaction note
		postHandler(
				`${this.path}data/:event[id]/interactions/:interaction[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<NoteT> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');

					const note: NoteI = await this.insertNote(
							req,
							interaction,
							undefined,
							async (
									event: IEvent,
									context: IInteraction|IPerson,
									req2: ICommonsRequestWithStrictParams
							): Promise<void> => {
								if (postNewInteractionNoteCallback) {
									await postNewInteractionNoteCallback(
											event,
											context,
											req2
									);
								}
							}
					);

					return this.encodeINote(note);
				}
		);

		// add person note
		// event is needed for Note even though it isn't for Person
		postHandler(
				`${this.path}data/:event[id]/persons/:person[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<NoteT> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const person: IInternalPerson|undefined = await this.personModel.getByIdOnly(req.strictParams.person as number);
					if (!person) return commonsRestApiNotFound('No such person exists');

					const note: NoteI = await this.insertNote(
							req,
							undefined,
							person,
							async (
									event: IEvent,
									context: IInteraction|IPerson,
									req2: ICommonsRequestWithStrictParams
							): Promise<void> => {
								if (postNewPersonNoteCallback) {
									await postNewPersonNoteCallback(
											event,
											context,
											req2
									);
								}
							}
					);

					return this.encodeINote(note);
				}
		);

		// edit interaction or person note
		patchHandler(
				`${this.path}data/:event[id]/uids/:uid[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<true> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');
			
					const existing: NoteI|undefined = await this.noteModel.getByUid(req.strictParams.uid as string);
					if (!existing) return commonsRestApiNotFound('No such note exists');
					if (existing.event !== event.id) return commonsRestApiConflict('Note vs event mismatch');
			
					if (isIInteractionNote<NoteI>(existing)) {
						const interaction: IInteraction|undefined = await this.interactionModel.getByFirstClassAndId(event, existing.interaction);
						if (!interaction) return commonsRestApiConflict('Note vs interaction ID mismatch');
			
						if (interaction.event !== event.id) return commonsRestApiConflict('Interaction mismatch');
					} else if (isIPersonNote<NoteI>(existing)) {
						const person: IPerson|undefined = await this.personModel.getByFirstClassAndId(event, existing.person);
						if (!person) return commonsRestApiConflict('Note vs person ID mismatch');
			
						if (person.event !== event.id) return commonsRestApiConflict('Person mismatch');
					} else {
						return commonsRestApiNotImplemented('Note is neither interaction or person');
					}
			
					const body: TEncodedObject = req.body as TEncodedObject;
					if (!commonsTypeHasProperty(body, 'value')) return commonsRestApiBadRequest('No body value supplied');
			
					const clone: NoteI = {
							...existing,
							value: body['value'] as string
					};
			
					await this.noteModel.updateForFirstClass(event, clone);

					if (postUpdateNoteCallback) {
						await postUpdateNoteCallback(
								event,
								existing,	// this is technically the previous value, but we only use it for UID/ID etc., so it doesn't matter
								req
						);
					}
			
					return true;
				}
		);

		// delete note
		deleteHandler(
				`${this.path}data/:event[id]/uids/:uid[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<boolean> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(req.strictParams.event as number);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const note: NoteI|undefined = await this.noteModel.getByUid(req.strictParams['uid'] as string);
					if (!note) return commonsRestApiNotFound('No such note exists');
					if (note.event !== event.id) return commonsRestApiConflict('Note vs event mismatch');
			
					await this.noteModel.deleteForFirstClass(
							event,
							note
					);
					
					this.deletedCallback(note);
					
					if (postDeleteNoteCallback) {
						await postDeleteNoteCallback(
								event,
								note,
								req
						);
					}
					
					return true;
				}
		);
	}
}
