import * as express from 'express';

import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ManagedOrderedApi } from 'nodecommons-es-rest-adamantine';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { TNamespace, encodeINamespace } from 'person-tracker-ts-assets';
import { TEncodedObject } from 'tscommons-es-core/dist';

import { IInternalNamespace } from '../models/namespace.model';
import { NamespaceModel } from '../models/namespace.model';

export class NamespaceApiHelper {
	constructor(
			private path: string,
			protected namespaceModel: NamespaceModel
	) {}
	
	public applyData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		const managedApi: ManagedOrderedApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalNamespace,
				NamespaceModel,
				TNamespace & TEncodedObject
		> = new ManagedOrderedApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalNamespace,
				NamespaceModel,
				TNamespace & TEncodedObject
		>(
				this.path,
				this.namespaceModel,
				encodeINamespace
		);
		
		managedApi.apply(
				// getHandler
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => getHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => postHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => putHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => patchHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => deleteHandler(
						query,
						handler
				)
		);
	}
}
