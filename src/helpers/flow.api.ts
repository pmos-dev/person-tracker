import * as express from 'express';

import { commonsTypeHasPropertyEnum } from 'tscommons-es-core';
import { ECommonsMoveDirection, isECommonsMoveDirection, toECommonsMoveDirection } from 'tscommons-es-models';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiNotFound,
		commonsRestApiBadRequest
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { TFlow, encodeIFlow } from 'person-tracker-ts-assets';

import { StepModel, IInternalStep } from '../models/step.model';
import { FlowModel, IInternalFlow } from '../models/flow.model';

export class FlowApiHelper {
	constructor(
			private path: string,
			protected stepModel: StepModel,
			protected flowModel: FlowModel
	) {}
	
	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		getHandler(
				`${this.path}data/:step[id]/ids`,
				async (eReq: express.Request, _res: express.Response): Promise<number[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(req.strictParams.step as number);
					if (!step) return commonsRestApiNotFound('No such step exists');
					
					const flows: IInternalFlow[] = await this.flowModel.listOrderedByFirstClass(step);
					
					return flows
							.map((flow: IInternalFlow): number => flow.id);
				}
		);
		
		getHandler(
				`${this.path}data/:step[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TFlow> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(req.strictParams.step as number);
					if (!step) return commonsRestApiNotFound('No such step exists');
					
					const flow: IInternalFlow|undefined = await this.flowModel.getByFirstClassAndId(step, req.strictParams.id as number);
					if (!flow) return commonsRestApiNotFound('No such flow exists');
					
					return encodeIFlow(flow);
				}
		);
	}
	
	public applyWriteData(
			_postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		putHandler(
				`${this.path}data/:step[id]/outsteps/:dest[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TFlow> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(req.strictParams.step as number);
					if (!step) return commonsRestApiNotFound('No such step exists');
					
					const dest: IInternalStep|undefined = await this.stepModel.getByIdOnly(req.strictParams.dest as number);
					if (!dest) return commonsRestApiNotFound('No such outflow step exists');
					
					const flow: IInternalFlow = await this.flowModel.insertForFirstClass(
							step,
							{
									outstep: dest.id
							}
					);
					
					return encodeIFlow(flow);
				}
		);
		
		patchHandler(
				`${this.path}data/:step[id]/ids/:id[id]/move`,
				async (eReq: express.Request, _res: express.Response): Promise<TFlow> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(req.strictParams.step as number);
					if (!step) return commonsRestApiNotFound('No such step exists');
					
					const flow: IInternalFlow|undefined = await this.flowModel.getByFirstClassAndId(
							step,
							req.strictParams.id as number
					);
					if (!flow) return commonsRestApiNotFound('No such flow exists');
					
					if (!commonsTypeHasPropertyEnum<ECommonsMoveDirection>(req.body, 'direction', isECommonsMoveDirection)) return commonsRestApiBadRequest('Invalid move direction');
					const direction: ECommonsMoveDirection = toECommonsMoveDirection(req.body.direction as string)!;
					switch (direction) {
						case ECommonsMoveDirection.UP:
						case ECommonsMoveDirection.DOWN:
						case ECommonsMoveDirection.TOP:
						case ECommonsMoveDirection.BOTTOM:
							try {
								const updated: IInternalFlow = await this.flowModel.move(
										flow,
										direction
								);
								
								return encodeIFlow(updated);
							} catch (e) {
								if (/^Cannot move /.test((e as Error).message)) {
									return commonsRestApiBadRequest((e as Error).message);
								}
								
								throw e;
							}
							
						default:
							return commonsRestApiBadRequest('Unknown movement direction');
					}
				}
		);
		
		deleteHandler(
				`${this.path}data/:step[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<boolean> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(req.strictParams.step as number);
					if (!step) return commonsRestApiNotFound('No such step exists');
					
					const flow: IInternalFlow|undefined = await this.flowModel.getByFirstClassAndId(
							step,
							req.strictParams.id as number
					);
					if (!flow) return commonsRestApiNotFound('No such flow exists');
					
					await this.flowModel.deleteForFirstClass(
							step,
							flow
					);
					
					return true;
				}
		);
	}
}
