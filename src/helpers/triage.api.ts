import * as express from 'express';

import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ManagedSecondClassApi } from 'nodecommons-es-rest-adamantine';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { TTriage, encodeITriage } from 'person-tracker-ts-assets';

import { IInternalNamespace } from '../models/namespace.model';
import { TriageModel, IInternalTriage } from '../models/triage.model';
import { StepModel } from '../models/step.model';

export class TriageApiHelper {
	constructor(
			private path: string,
			protected triageModel: TriageModel,
			protected stepModel: StepModel
	) {}
	
	public applyData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		const managedApi: ManagedSecondClassApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalTriage,
				IInternalNamespace,
				TriageModel,
				TTriage
		> = new ManagedSecondClassApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalTriage,
				IInternalNamespace,
				TriageModel,
				TTriage
		>(
				this.path,
				this.triageModel,
				encodeITriage,
				async (p: IInternalNamespace, m: IInternalTriage): Promise<void> => {
					await this.postCreateTriage(p, m);
				}
		);
		
		managedApi.apply(
				// getHandler
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => getHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => postHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => putHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => patchHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => deleteHandler(
						query,
						handler
				)
		);
	}
	
	private async postCreateTriage(_namespace: IInternalNamespace, triage: IInternalTriage): Promise<void> {
		await this.stepModel.createRoot(triage);
	}
}
