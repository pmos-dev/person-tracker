import * as express from 'express';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import {
		commonsRestApiError,
		commonsRestApiNotFound
} from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { TInteraction, encodeIInteraction } from 'person-tracker-ts-assets';

import { EventModel, IInternalEvent } from '../models/event.model';
import { InteractionModel, IInternalInteraction } from '../models/interaction.model';

export class InteractionApiHelper {
	constructor(
			private path: string,
			protected eventModel: EventModel,
			protected interactionModel: InteractionModel
	) {}
	
	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		getHandler(
				`${this.path}data/:event[id]/ids`,
				async (eReq: express.Request, _res: express.Response): Promise<number[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');
					
					const interactions: IInternalInteraction[] = await this.interactionModel.listByFirstClass(event);

					return interactions
							.map((interaction: IInternalInteraction): number => interaction.id);
				}
		);
		
		getHandler(
				`${this.path}data/:event[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TInteraction[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');
					
					const interactions: IInternalInteraction[] = await this.interactionModel.listByFirstClass(event);

					return interactions
							.map((interaction: IInternalInteraction): TInteraction => encodeIInteraction(interaction));
				}
		);
		
		getHandler(
				`${this.path}data/:event[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TInteraction> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');
					
					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByFirstClassAndId(
							event,
							req.strictParams.id as number
					);
					if (!interaction) return commonsRestApiError('No such interaction exists');
					
					return encodeIInteraction(interaction);
				}
		);
		
		getHandler(
				`${this.path}data/uids/:uid[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<TInteraction> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByUid(req.strictParams.uid as string);
					if (!interaction) return commonsRestApiError('No such interaction exists');
					
					return encodeIInteraction(interaction);
				}
		);
	}
	
	public applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postNewCallback?: (
					event: IInternalEvent,
					interaction: IInternalInteraction,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>,
			postDeleteCallback?: (
					event: IInternalEvent,
					interaction: IInternalInteraction,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): void {
		postHandler(
				`${this.path}data/:event[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<TInteraction> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');
					
					const interaction: IInternalInteraction = await this.interactionModel.createForEvent(event);

					if (postNewCallback) {
						await postNewCallback(
								event,
								interaction,
								req
						);
					}
					
					return encodeIInteraction(interaction);
				}
		);
		
		deleteHandler(
				`${this.path}data/:event[id]/ids/:id[id]`,
				async (eReq: express.Request, _res: express.Response): Promise<boolean> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const event: IInternalEvent|undefined = await this.eventModel.getByIdOnly(
							req.strictParams.event as number
					);
					if (!event) return commonsRestApiNotFound('No such event exists');

					const existing: IInternalInteraction|undefined = await this.interactionModel.getByFirstClassAndId(
							event,
							req.strictParams.id as number
					);
					if (!existing) return commonsRestApiNotFound('No such interaction exists');
					
					await this.interactionModel.deleteForFirstClass(event, existing);

					if (postDeleteCallback) {
						await postDeleteCallback(
								event,
								existing,
								req
						);
					}

					return true;
				}
		);
	}
}
