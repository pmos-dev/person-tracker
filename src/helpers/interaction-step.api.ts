import * as express from 'express';

import { commonsDateDateToYmdHis, commonsDateYmdHisToDate, commonsTypeHasPropertyString, COMMONS_REGEX_PATTERN_DATETIME_YMDHIS } from 'tscommons-es-core';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { commonsRestApiBadRequest, commonsRestApiNotFound } from 'nodecommons-es-rest';
import { TCommonsContentHandler } from 'nodecommons-es-rest';

import { InteractionModel } from '../models/interaction.model';
import { IInternalInteraction } from '../models/interaction.model';
import { IInternalInteractionStep, InteractionStepModel } from '../models/interaction-step.model';
import { IInternalStep, StepModel } from '../models/step.model';

export class InteractionStepApiHelper {
	constructor(
			private path: string,
			protected interactionModel: InteractionModel,
			protected stepModel: StepModel,
			protected interactionStepModel: InteractionStepModel
	) {}

	public applyReadData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		getHandler(
				`${this.path}interactions/:interaction[id]/steps`,
				async (eReq: express.Request, _res: express.Response): Promise<{
						step: string;
						timestamp: string;
				}[]> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;
					
					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');

					const interactionSteps: IInternalInteractionStep[] = await this.interactionStepModel.listByInteraction(interaction);

					return await Promise.all(
							interactionSteps
									.map(async (interactionStep: IInternalInteractionStep): Promise<{
											id: number;
											step: string;
											timestamp: string;
									}> => {
										const step: IInternalStep|undefined = await this.stepModel.getByIdOnly(interactionStep.step);
										if (!step) throw new Error('No such step for that interactionStep. This should not be possible due to foreign keys');

										return {
												id: interactionStep.id,	// this is only needed so that caching can tell the difference between multiple traverals to the same step in the history
												step: step.uid,
												timestamp: commonsDateDateToYmdHis(interactionStep.timestamp, true)
										};
									})
					);
				}
		);
	}
	
	public applyWriteData(
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			_deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postLinkCallback?: (
					interaction: IInternalInteraction,
					step: IInternalStep,
					timestamp: Date,
					req: ICommonsRequestWithStrictParams
			) => Promise<void>
	): void {
		postHandler(
				`${this.path}interactions/:interaction[id]/steps/:step[base62]`,
				async (eReq: express.Request, _res: express.Response): Promise<true> => {
					const req: ICommonsRequestWithStrictParams = eReq as ICommonsRequestWithStrictParams;

					const interaction: IInternalInteraction|undefined = await this.interactionModel.getByIdOnly(req.strictParams.interaction as number);
					if (!interaction) return commonsRestApiNotFound('No such interaction exists');

					const step: IInternalStep|undefined = await this.stepModel.getByUid(req.strictParams.step as string);
					if (!step) return commonsRestApiNotFound('No such step exists');

					if (!commonsTypeHasPropertyString(req.body, 'timestamp') || !COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(req.body.timestamp as string)) return commonsRestApiBadRequest('No timestamp supplied');
					const timestamp: Date = commonsDateYmdHisToDate(req.body.timestamp as string, true);

					await this.interactionStepModel.createForInteraction(
							interaction,
							step,
							timestamp
					);
					
					if (postLinkCallback) {
						await postLinkCallback(
								interaction,
								step,
								timestamp,
								req
						);
					}

					return true;
				}
		);
	}
}
