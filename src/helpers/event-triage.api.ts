import * as express from 'express';

import { TEncodedObject } from 'tscommons-es-core';
import { ICommonsM2MLink } from 'tscommons-es-models';

import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ManagedM2MLinkTableApi } from 'nodecommons-es-rest-adamantine';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { EventTriageModel } from '../models/event-triage.model';
import { IInternalEvent } from '../models/event.model';
import { IInternalTriage } from '../models/triage.model';

export class EventTriageApiHelper {
	constructor(
			private path: string,
			protected eventTriageModel: EventTriageModel
	) {}
	
	public applyData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		const m2mApi: ManagedM2MLinkTableApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalEvent,
				IInternalTriage,
				ICommonsM2MLink<IInternalEvent, IInternalTriage>,
				EventTriageModel,
				TEncodedObject
		> = new ManagedM2MLinkTableApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalEvent,
				IInternalTriage,
				ICommonsM2MLink<IInternalEvent, IInternalTriage>,
				EventTriageModel,
				TEncodedObject
		>(
				this.path,
				this.eventTriageModel,
				(eventTriage: ICommonsM2MLink<IInternalEvent, IInternalTriage>): TEncodedObject => eventTriage as TEncodedObject
		);
		
		m2mApi.apply(
				// getHandler
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => getHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => postHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => putHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => patchHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => deleteHandler(
						query,
						handler
				)
		);
	}
}
