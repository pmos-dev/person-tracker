import * as express from 'express';

import { TCommonsContentHandler } from 'nodecommons-es-rest';
import { ManagedOrientatedOrderedApi } from 'nodecommons-es-rest-adamantine';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { TField, encodeIField } from 'person-tracker-ts-assets';

import { IInternalNamespace } from '../models/namespace.model';
import { FieldModel, IInternalField } from '../models/field.model';

export class FieldApiHelper {
	constructor(
			private path: string,
			protected fieldModel: FieldModel
	) {}
	
	public applyData(
			getHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			postHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			putHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			patchHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void,
			deleteHandler: (query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>) => void
	): void {
		const managedApi: ManagedOrientatedOrderedApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalField,
				IInternalNamespace,
				FieldModel,
				TField
		> = new ManagedOrientatedOrderedApi<
				ICommonsRequestWithStrictParams,
				express.Response,
				IInternalField,
				IInternalNamespace,
				FieldModel,
				TField
		>(
				this.path,
				this.fieldModel,
				encodeIField
		);
		
		managedApi.apply(
				// getHandler
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => getHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => postHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => putHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => patchHandler(
						query,
						handler
				),
				(query: string, handler: TCommonsContentHandler<ICommonsRequestWithStrictParams, express.Response>): void => deleteHandler(
						query,
						handler
				)
		);
	}
}
