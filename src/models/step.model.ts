import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import { CommonsDatabaseTypeId, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';
import { CommonsModelForeignKey } from 'nodecommons-es-models';
import { CommonsStepFlowStepModel } from 'nodecommons-es-models-step-flow';

import { IStep } from 'person-tracker-ts-assets';

import { TriageModel, IInternalTriage } from './triage.model';
import { FieldModel, IInternalField } from './field.model';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IInternalStep extends IStep {}

export class StepModel extends CommonsStepFlowStepModel<
		IInternalStep,
		IInternalTriage
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			triageModel: TriageModel,
			fieldModel: FieldModel
	) {
		super(
				database,
				'steps',
				{
						field: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL)
				},
				triageModel,
				'triage',
				{
						field: new CommonsModelForeignKey(
								fieldModel,
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						)
				}
		);
	}
	
	public async createRoot(
			triage: IInternalTriage
	): Promise<IInternalStep> {
		return await super.create(
				triage,
				ECommonsStepFlowStepType.ROOT,
				{}
		);
	}
	
	public async createField(
			triage: IInternalTriage,
			field: IInternalField
	): Promise<IInternalStep> {
		return await super.create(
				triage,
				ECommonsStepFlowStepType.DATA,
				{
						field: field.id
				}
		);
	}
	
	public async createComplete(triage: IInternalTriage): Promise<IInternalStep> {
		return await super.create(
				triage,
				ECommonsStepFlowStepType.COMPLETE,
				{}
		);
	}
	
	public async createAbort(triage: IInternalTriage): Promise<IInternalStep> {
		return await super.create(
				triage,
				ECommonsStepFlowStepType.ABORT,
				{}
		);
	}
}
