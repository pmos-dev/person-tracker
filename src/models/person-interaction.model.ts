import { ICommonsM2MLink } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsManagedM2MLinkTable } from 'nodecommons-es-models-adamantine';

import { PersonModel, IInternalPerson } from './person.model';
import { InteractionModel, IInternalInteraction } from './interaction.model';

export class PersonInteractionModel extends CommonsManagedM2MLinkTable<
		ICommonsM2MLink<IInternalPerson, IInternalInteraction>,
		IInternalPerson,
		IInternalInteraction
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			personModel: PersonModel,
			interactionModel: InteractionModel
	) {
		super(
				database,
				'person_interactions',
				personModel,
				interactionModel,
				'person',
				'interaction',
				{}
		);
	}

}
