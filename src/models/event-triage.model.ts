import { ICommonsM2MLink } from 'tscommons-es-models';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsManagedM2MLinkTable } from 'nodecommons-es-models-adamantine';

import { EventModel, IInternalEvent } from './event.model';
import { TriageModel, IInternalTriage } from './triage.model';

export class EventTriageModel extends CommonsManagedM2MLinkTable<
		ICommonsM2MLink<IInternalEvent, IInternalTriage>,
		IInternalEvent,
		IInternalTriage
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			eventModel: EventModel,
			triageModel: TriageModel
	) {
		super(
				database,
				'event_triages',
				eventModel,
				triageModel,
				'event',
				'triage',
				{}
		);
	}

}
