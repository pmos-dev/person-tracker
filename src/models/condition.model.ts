import { commonsTypeIsString } from 'tscommons-es-core';
import { TCommonsStepFlowCondition } from 'tscommons-es-models-step-flow';

import { CommonsDatabaseTypeText, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsStepFlowConditionModel } from 'nodecommons-es-models-step-flow';

import { ICondition } from 'person-tracker-ts-assets';

import { FlowModel, IInternalFlow } from './flow.model';
import { IInternalStep } from './step.model';
import { IInternalTriage } from './triage.model';

export interface IInternalCondition extends TCommonsStepFlowCondition {
		value?: string;
}

export function encodeIInternalCondition(condition: ICondition): IInternalCondition {
	return {
			...condition,
			value: condition.value === undefined ? undefined : JSON.stringify(condition.value)
	};
}

export function decodeIInternalCondition(encoded: IInternalCondition): ICondition {
	const attempt: unknown|undefined = encoded.value === undefined ? undefined : JSON.parse(encoded.value);
	if (attempt !== undefined && !commonsTypeIsString(attempt)) throw new Error('Invalid valie for IInternalCondition decoding');
	
	return {
			...encoded,
			value: attempt
	};
}

export class ConditionModel extends CommonsStepFlowConditionModel<
		IInternalCondition,
		IInternalFlow,
		IInternalStep,
		IInternalTriage
> {
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			flowModel: FlowModel
	) {
		super(
				database,
				'conditions',
				{
						value: new CommonsDatabaseTypeText(ECommonsDatabaseTypeNull.ALLOW_NULL)
				},
				flowModel,
				'flow'
		);
	}
	
	public async getByFlowAndId(
			flow: IInternalFlow,
			id: number
	): Promise<ICondition|undefined> {
		const condition: IInternalCondition|undefined = await this.getByFirstClassAndId(
				flow,
				id
		);
		if (!condition) return undefined;
		
		return decodeIInternalCondition(condition);
	}
	
	public async listByFlow(flow: IInternalFlow): Promise<ICondition[]> {
		const conditions: IInternalCondition[] = await this.listByFirstClass(flow);
		
		return conditions
				.map((condition: IInternalCondition): ICondition => decodeIInternalCondition(condition));
	}
	
	public async insertForFlow(
			flow: IInternalFlow,
			data: Omit<ICondition, 'id' | 'flow'>
	): Promise<IInternalCondition> {
		return await this.insertForFirstClass(
				flow,
				encodeIInternalCondition(data as ICondition)	// fudge
		);
	}
}
