import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsManagedOrdered } from 'nodecommons-es-models-adamantine';

import { INamespace } from 'person-tracker-ts-assets';

// ridiculous hack because TypeScript can't seem to realise that INamespace is already a subinterface of ICommonsManaged
export interface IInternalNamespace extends INamespace { [ key: string ]: any }

export class NamespaceModel extends CommonsManagedOrdered<IInternalNamespace> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>
	) {
		super(
				database,
				'namespaces',
				{}
		);
	}
	
	//-------------------------------------------------------------------------

}
