import { CommonsDatabaseTypeEnum, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsManagedOrientatedOrdered } from 'nodecommons-es-models-adamantine';
import { CommonsDataFieldHelper } from 'nodecommons-es-models-field';

import { EDataMethod, EDATA_METHODS, fromEDataMethod, IField, toEDataMethod } from 'person-tracker-ts-assets';
import { EFieldContext, fromEFieldContext, toEFieldContext, EFIELD_CONTEXTS } from 'person-tracker-ts-assets';

import { NamespaceModel, IInternalNamespace } from './namespace.model';

// ridiculous hack because TypeScript can't seem to realise that IField is already a subinterface of ICommonsManaged
export interface IInternalField extends IField { [ key: string ]: any }

export class FieldModel extends CommonsManagedOrientatedOrdered<
		IInternalField,
		IInternalNamespace
> {

	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			namespaceModel: NamespaceModel
	) {
		super(
				database,
				'fields',
				CommonsDataFieldHelper.extendStructure(
						{
								context: new CommonsDatabaseTypeEnum<EFieldContext>(
										EFIELD_CONTEXTS,
										fromEFieldContext,
										toEFieldContext,
										ECommonsDatabaseTypeNull.NOT_NULL,
										undefined,
										undefined,
										'PersonTracker_Field_Context'
								),
								method: new CommonsDatabaseTypeEnum<EDataMethod>(
										EDATA_METHODS,
										fromEDataMethod,
										toEDataMethod,
										ECommonsDatabaseTypeNull.NOT_NULL,
										undefined,
										undefined,
										'PersonTracker_Field_DataMethod'
								)
						},
						'PersonTracker_Field_Type'
				),
				namespaceModel,
				'namespace',
				undefined,
				undefined,
				undefined,
				false,
				true
		);
	}

}
