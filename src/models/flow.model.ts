import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsStepFlowFlowModel } from 'nodecommons-es-models-step-flow';

import { IFlow } from 'person-tracker-ts-assets';

import { StepModel, IInternalStep } from './step.model';
import { IInternalTriage } from './triage.model';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IInternalFlow extends IFlow {}

export class FlowModel extends CommonsStepFlowFlowModel<
		IInternalFlow,
		IInternalStep,
		IInternalTriage
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			stepModel: StepModel
	) {
		super(
				database,
				'flows',
				stepModel,
				'step'
		);
	}
}
