import { commonsBase62GenerateRandomId } from 'tscommons-es-core';
import { CommonsSemaphore } from 'tscommons-es-async';

import {
		CommonsDatabaseType,
		CommonsDatabaseTypeBase62BigId,
		CommonsDatabaseTypeBigInt,
		CommonsDatabaseTypeDateTime,
		CommonsDatabaseTypeId,
		CommonsDatabaseTypeText,
		CommonsDatabaseUniqueKey,
		ECommonsDatabaseTypeSigned,
		ICommonsCredentials
} from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';
import { CommonsModelForeignKey } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsUniqueNamedHelper } from 'nodecommons-es-models-adamantine';

import { INote, IInteractionNote, IPersonNote, TInteractionNote, TPersonNote } from 'person-tracker-ts-assets';

import { IInternalPerson, PersonModel } from './person.model';
import { IInternalInteraction, InteractionModel } from './interaction.model';
import { EventModel, IInternalEvent } from './event.model';

// fudgy way of getting a valid generic interface via a type
// This is just syntactic sugar to match the syntax convention used in IInternalData; it doesn't actually do anything
export type IInternalNote<NoteI extends INote> = NoteI;
export type IInternalInteractionNote<NoteI extends INote> = IInternalNote<NoteI> & TInteractionNote;
export type IInternalPersonNote<NoteI extends INote> = IInternalNote<NoteI> & TPersonNote;

export class NoteModel<NoteI extends INote = INote> extends CommonsSecondClass<
		IInternalNote<NoteI>,
		IInternalEvent
> {
	private lognoTransactionSemaphore: CommonsSemaphore = new CommonsSemaphore();
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			eventModel: EventModel,
			personModel: PersonModel,
			interactionModel: InteractionModel,
			additionalFields: { [ key: string ]: CommonsDatabaseType } = {},
			additionalForeignKeys: { [ key: string ]: CommonsModelForeignKey } = {},
			additionalUniqueKeys: CommonsDatabaseUniqueKey[] = []
	) {
		super(
				database,
				'notes',
				{
						logno: new CommonsDatabaseTypeBigInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
						uid: new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL),
						timestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
						person: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL),
						interaction: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL),
						value: new CommonsDatabaseTypeText(ECommonsDatabaseTypeNull.NOT_NULL),
						...additionalFields
				},
				eventModel,
				'event',
				{
						person: new CommonsModelForeignKey(
								personModel,
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						),
						interaction: new CommonsModelForeignKey(
								interactionModel,
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						),
						...additionalForeignKeys
				},
				[
						new CommonsDatabaseUniqueKey([ 'uid' ], 'notes_uid'),
						new CommonsDatabaseUniqueKey([ 'event', 'logno' ], 'notes_event_logno'),
						...additionalUniqueKeys
				]
		);
	}

	protected override preprepare(): void {
		super.preprepare();

		CommonsUniqueNamedHelper.build<IInternalNote<NoteI>>(this.database)
				.buildPreprepareForFirstClass(
						this,
						'uid',
						true
				);

		this.database.preprepare(
				'PersonTracker__Note__GET_LOGNO',
				`
					SELECT MAX(${this.modelField(this, 'logno')}) AS "logno"
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
				`,
				{
						event: this.structure['event']
				},
				{
						logno: new CommonsDatabaseTypeBigInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.ALLOW_NULL)
				}
		);

		this.database.preprepare(
				'PersonTracker__Note__LIST_BY_EVENT_SINCE_LOGNO',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
					AND ${this.modelField(this, 'logno')} > :logno
					ORDER BY ${this.modelField(this, 'timestamp')} ASC
				`,
				{
						event: this.structure['event'],
						logno: this.structure['logno']
				},
				this.structure
		);

		this.database.preprepare(
				'PersonTracker__Note__LIST_BY_EVENT_AND_INTERACTION',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
					AND ${this.modelField(this, 'interaction')} = :interaction
					ORDER BY ${this.modelField(this, 'timestamp')} ASC
				`,
				{
						event: this.structure['event'],
						interaction: this.structure['interaction']
				},
				this.structure
		);

		this.database.preprepare(
				'PersonTracker__Note__LIST_BY_EVENT_AND_PERSON',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
					AND ${this.modelField(this, 'person')} = :person
					ORDER BY ${this.modelField(this, 'timestamp')} ASC
				`,
				{
						event: this.structure['event'],
						person: this.structure['person']
				},
				this.structure
		);
	}
	
	//-------------------------------------------------------------------------

	public async getLogno(event: IInternalEvent): Promise<number> {
		const logno: number|undefined = await this.database.executeParamsValue<number>(
				'PersonTracker__Note__GET_LOGNO',
				{
						event: event.id
				},
				true
		 );

		 return logno || 0;
	}

	public async listByEvent(event: IInternalEvent): Promise<NoteI[]> {
		const notes: IInternalNote<NoteI>[] = await super.listByFirstClass(event);

		return notes;
	}
	
	public async listByEventSinceLogno(
			event: IInternalEvent,
			logno: number
	): Promise<NoteI[]> {
		const notes: IInternalNote<NoteI>[] = await this.database.executeParams<IInternalNote<NoteI>>(
				'PersonTracker__Note__LIST_BY_EVENT_SINCE_LOGNO',
				{
						event: event.id,
						logno: logno
				}
		 );

		 return notes;
	}

	public async listByInteraction(
			event: IInternalEvent,
			interaction: IInternalInteraction
	): Promise<IInteractionNote<NoteI>[]> {
		const notes: IInternalInteractionNote<NoteI>[] = await this.database.executeParams<IInternalInteractionNote<NoteI>>(
				'PersonTracker__Note__LIST_BY_EVENT_AND_INTERACTION',
				{
						event: event.id,
						interaction: interaction.id
				}
		);

		return notes;
	}

	public async listByPerson(
			event: IInternalEvent,
			person: IInternalPerson
	): Promise<IPersonNote<NoteI>[]> {
		const notes: IInternalPersonNote<NoteI>[] = await this.database.executeParams<IInternalPersonNote<NoteI>>(
				'PersonTracker__Note__LIST_BY_EVENT_AND_PERSON',
				{
						event: event.id,
						person: person.id
				}
		);

		return notes;
	}

	public async getByUid(uid: string): Promise<NoteI|undefined> {
		const note: IInternalNote<NoteI>|undefined = await CommonsUniqueNamedHelper.build<IInternalNote<NoteI>>(this.database)
				.getFirstClassByName(
						this,
						uid,
						'uid'
				);

		return note;
	}
	
	//-------------------------------------------------------------------------

	public async insertInteractionLogEntry(
			event: IInternalEvent,
			interaction: IInternalInteraction,
			timestamp: Date,
			value: string,
			additionalValues: { [ key: string ]: unknown } = {}
	): Promise<IInteractionNote<NoteI>> {
		// we need this semaphore as the logno get is not transactional against the insert, so a key violation could occur if multiple transactions are running at once
		try {
			await this.lognoTransactionSemaphore.claim(2000);
		} catch (e) {
			// unable to claim semaphore in 2000ms.
			throw new Error('Unable to claim semaphore for logono insertInteractionLogEntry in 2000ms');
		}
		try {
			const logno: number = await this.getLogno(event) + 1;

			const note: Omit<
					IInternalInteractionNote<NoteI>,
					'event' | 'id'
			// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
			> = {
					uid: commonsBase62GenerateRandomId(),
					logno: logno,
					timestamp: timestamp,
					value: value,
					interaction: interaction.id,
					...additionalValues
			} as unknown as Omit<
					IInternalInteractionNote<NoteI>,
					'event' | 'id'
			>;

			const inserted: IInternalInteractionNote<NoteI> = await this.insertForFirstClass(
					event,
					note
			) as IInternalInteractionNote<NoteI>;

			return inserted;
		} finally {
			this.lognoTransactionSemaphore.release();
		}
	}

	public async insertPersonLogEntry(
			event: IInternalEvent,
			person: IInternalPerson,
			timestamp: Date,
			value: string,
			additionalValues: { [ key: string ]: unknown } = {}
	): Promise<IPersonNote<NoteI>> {
		// we need this semaphore as the logno get is not transactional against the insert, so a key violation could occur if multiple transactions are running at once
		try {
			await this.lognoTransactionSemaphore.claim(2000);
		} catch (e) {
			// unable to claim semaphore in 2000ms.
			throw new Error('Unable to claim semaphore for logono insertInteractionLogEntry in 2000ms');
		}
		try {
			const logno: number = await this.getLogno(event) + 1;

			const note: Omit<
					IInternalPersonNote<NoteI>,
					'event' | 'id'
			// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
			> = {
					uid: commonsBase62GenerateRandomId(),
					logno: logno,
					timestamp: timestamp,
					value: value,
					person: person.id,
					...additionalValues
			} as unknown as Omit<
					IInternalPersonNote<NoteI>,
					'event' | 'id'
			>;

			const inserted: IInternalPersonNote<NoteI> = await this.insertForFirstClass(
					event,
					note
			) as IInternalPersonNote<NoteI>;

			return inserted;
		} finally {
			this.lognoTransactionSemaphore.release();
		}
	}
}
