/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import {
		commonsBase62GenerateRandomId,
		commonsDateDateToHis,
		commonsDateDateToYmd,
		commonsDateDateToYmdHis,
		commonsDateHisToDate,
		commonsDateYmdHisToDate,
		commonsDateYmdToDate,
		commonsTypeAttemptBoolean,
		commonsTypeAttemptNumber,
		commonsTypeIsDate,
		commonsTypeIsNumber,
		commonsTypeIsString,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		COMMONS_REGEX_PATTERN_DATE_YMD,
		COMMONS_REGEX_PATTERN_TIME_HIS
} from 'tscommons-es-core';
import { CommonsSemaphore } from 'tscommons-es-async';
import { ECommonsAdamantineFieldType } from 'tscommons-es-models-field';

import {
		CommonsDatabaseType,
		CommonsDatabaseTypeBase62BigId,
		CommonsDatabaseTypeBigInt,
		CommonsDatabaseTypeDateTime,
		CommonsDatabaseTypeId,
		CommonsDatabaseTypeText,
		CommonsDatabaseUniqueKey,
		ECommonsDatabaseTypeSigned,
		ICommonsCredentials
} from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { ECommonsDatabaseReferentialAction } from 'nodecommons-es-database';
import { CommonsModelForeignKey } from 'nodecommons-es-models';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsUniqueNamedHelper } from 'nodecommons-es-models-adamantine';

import { IData, IEvent, IField, IInteractionData, IPersonData, TInteractionData, TPersonData } from 'person-tracker-ts-assets';

import { FieldModel, IInternalField } from './field.model';
import { IInternalPerson, PersonModel } from './person.model';
import { IInternalInteraction, InteractionModel } from './interaction.model';
import { EventModel, IInternalEvent } from './event.model';

// fudgy way of getting a valid generic interface via a type
export type IInternalData<DataI extends IData> = DataI & {
		value: string|undefined;
}
export type IInternalInteractionData<DataI extends IData> = IInternalData<DataI> & TInteractionData;
export type IInternalPersonData<DataI extends IData> = IInternalData<DataI> & TPersonData;

function encodeValue(
		field: IField,
		value: string|number|boolean|Date|undefined
): string|undefined {
	if (value === undefined) return undefined;

	switch (field.type) {
		case ECommonsAdamantineFieldType.DATE: {
			if (!commonsTypeIsDate(value)) return undefined;
			return `${ECommonsAdamantineFieldType.DATE}_${commonsDateDateToYmd(value, true)}`;
		}
		case ECommonsAdamantineFieldType.TIME: {
			if (!commonsTypeIsDate(value)) return undefined;
			return `${ECommonsAdamantineFieldType.TIME}_${commonsDateDateToHis(value, true)}`;
		}
		case ECommonsAdamantineFieldType.DATETIME: {
			if (!commonsTypeIsDate(value)) return undefined;
			return `${ECommonsAdamantineFieldType.DATETIME}_${commonsDateDateToYmdHis(value, true)}`;
		}
		case ECommonsAdamantineFieldType.ENUM:
		case ECommonsAdamantineFieldType.STRING: {
			if (!commonsTypeIsString(value)) return undefined;
			return `${ECommonsAdamantineFieldType.STRING}_${value}`;
		}
		case ECommonsAdamantineFieldType.BOOLEAN: {
			return `${ECommonsAdamantineFieldType.BOOLEAN}_${value ? 'true' : 'false'}`;
		}
		case ECommonsAdamantineFieldType.INT:
		case ECommonsAdamantineFieldType.SMALLINT:
		case ECommonsAdamantineFieldType.TINYINT: {
			if (!commonsTypeIsNumber(value)) return undefined;
			return `${ECommonsAdamantineFieldType.INT}_${value.toString(10)}`;
		}
		case ECommonsAdamantineFieldType.FLOAT: {
			if (!commonsTypeIsNumber(value)) return undefined;
			return `${ECommonsAdamantineFieldType.FLOAT}_${value.toString()}`;
		}
		default:
			throw new Error('Unimplemented data type to encode');
	}
}

function decodeValue(
		value: string|undefined
): string|number|boolean|Date|undefined {
	if (value === undefined) return undefined;

	const result: RegExpExecArray|null = /^([a-z]+)_(.*)$/.exec(value);
	if (!result) throw new Error('Unable to parse encoded database data');

	const type: string = result[1];
	value = result[2];

	switch (type) {
		case ECommonsAdamantineFieldType.DATE: {
			if (!commonsTypeIsString(value)) return undefined;
			if (!COMMONS_REGEX_PATTERN_DATE_YMD.test(value)) return undefined;
			return commonsDateYmdToDate(value, true);
		}
		case ECommonsAdamantineFieldType.TIME: {
			if (!commonsTypeIsString(value)) return undefined;
			if (!COMMONS_REGEX_PATTERN_TIME_HIS.test(value)) return undefined;
			return commonsDateHisToDate(value, true);
		}
		case ECommonsAdamantineFieldType.DATETIME: {
			if (!commonsTypeIsString(value)) return undefined;
			if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(value)) return undefined;
			return commonsDateYmdHisToDate(value, true);
		}
		case ECommonsAdamantineFieldType.ENUM:	// not actually a used type, but left here for consistency
		case ECommonsAdamantineFieldType.STRING: {
			if (!commonsTypeIsString(value)) return undefined;
			return value;
		}
		case ECommonsAdamantineFieldType.BOOLEAN: {
			return commonsTypeAttemptBoolean(value);
		}
		case ECommonsAdamantineFieldType.INT:
		case ECommonsAdamantineFieldType.SMALLINT:	// not actually a used type, but left here for consistency
		case ECommonsAdamantineFieldType.TINYINT:	// not actually a used type, but left here for consistency
		case ECommonsAdamantineFieldType.FLOAT: {
			return commonsTypeAttemptNumber(value);
		}
		default:
			throw new Error('Unimplemented data type to decode');
	}
}

export class DataModel<DataI extends IData = IData> extends CommonsSecondClass<
		IInternalData<DataI>,
		IInternalEvent
> {
	private lognoTransactionSemaphore: CommonsSemaphore = new CommonsSemaphore();
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			eventModel: EventModel,
			fieldModel: FieldModel,
			personModel: PersonModel,
			interactionModel: InteractionModel,
			additionalFields: { [ key: string ]: CommonsDatabaseType } = {},
			additionalForeignKeys: { [ key: string ]: CommonsModelForeignKey } = {},
			additionalUniqueKeys: CommonsDatabaseUniqueKey[] = []
	) {
		super(
				database,
				'datas',
				{
						logno: new CommonsDatabaseTypeBigInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
						uid: new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL),
						timestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
						field: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						person: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL),
						interaction: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL),
						value: new CommonsDatabaseTypeText(ECommonsDatabaseTypeNull.ALLOW_NULL),
						...additionalFields
				},
				eventModel,
				'event',
				{
						field: new CommonsModelForeignKey(
								fieldModel,
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						),
						person: new CommonsModelForeignKey(
								personModel,
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						),
						interaction: new CommonsModelForeignKey(
								interactionModel,
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						),
						...additionalForeignKeys
				},
				[
						new CommonsDatabaseUniqueKey([ 'uid' ], 'datas_uid'),
						new CommonsDatabaseUniqueKey([ 'event', 'logno' ], 'datas_event_logno'),
						...additionalUniqueKeys
				]
		);
	}

	protected override preprepare(): void {
		super.preprepare();

		CommonsUniqueNamedHelper.build<IInternalData<DataI>>(this.database)
				.buildPreprepareForFirstClass(
						this,
						'uid',
						true
				);

		this.database.preprepare(
				'PersonTracker__Data__GET_LOGNO',
				`
					SELECT MAX(${this.modelField(this, 'logno')}) AS "logno"
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
				`,
				{
						event: this.structure['event']
				},
				{
						logno: new CommonsDatabaseTypeBigInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.ALLOW_NULL)
				}
		);

		this.database.preprepare(
				'PersonTracker__Data__LIST_BY_EVENT_SINCE_LOGNO',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
					AND ${this.modelField(this, 'logno')} > :logno
					ORDER BY ${this.modelField(this, 'timestamp')} ASC
				`,
				{
						event: this.structure['event'],
						logno: this.structure['logno']
				},
				this.structure
		);

		this.database.preprepare(
				'PersonTracker__Data__LIST_BY_EVENT_AND_INTERACTION',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
					AND ${this.modelField(this, 'interaction')} = :interaction
					ORDER BY ${this.modelField(this, 'timestamp')} ASC
				`,
				{
						event: this.structure['event'],
						interaction: this.structure['interaction']
				},
				this.structure
		);

		this.database.preprepare(
				'PersonTracker__Data__LIST_BY_EVENT_AND_PERSON',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'event')} = :event
					AND ${this.modelField(this, 'person')} = :person
					ORDER BY ${this.modelField(this, 'timestamp')} ASC
				`,
				{
						event: this.structure['event'],
						person: this.structure['person']
				},
				this.structure
		);
	}
	
	//-------------------------------------------------------------------------
	
	public async getLogno(event: IInternalEvent): Promise<number> {
		const logno: number|undefined = await this.database.executeParamsValue<number>(
				'PersonTracker__Data__GET_LOGNO',
				{
						event: event.id
				},
				true
		 );

		 return logno || 0;
	}

	public async listByEvent(event: IInternalEvent): Promise<DataI[]> {
		const datas: IInternalData<DataI>[] = await super.listByFirstClass(event);

		return datas
				.map((d: IInternalData<DataI>): DataI => ({
						...d,
						value: decodeValue(d.value)
				}));
	}
	
	public async listByEventSinceLogno(
			event: IInternalEvent,
			logno: number
	): Promise<DataI[]> {
		const datas: IInternalData<DataI>[] = await this.database.executeParams<IInternalData<DataI>>(
				'PersonTracker__Data__LIST_BY_EVENT_SINCE_LOGNO',
				{
						event: event.id,
						logno: logno
				}
		 );

		 return datas
				.map((d: IInternalData<DataI>): DataI => ({
						...d,
						value: decodeValue(d.value)
				}));
	}

	public async listByInteraction(
			event: IInternalEvent,
			interaction: IInternalInteraction
	): Promise<IInteractionData<DataI>[]> {
		const datas: IInternalInteractionData<DataI>[] = await this.database.executeParams<IInternalInteractionData<DataI>>(
				'PersonTracker__Data__LIST_BY_EVENT_AND_INTERACTION',
				{
						event: event.id,
						interaction: interaction.id
				}
		);

		return datas
				.map((d: IInternalInteractionData<DataI>): IInteractionData<DataI> => ({
						...d,
						value: decodeValue(d.value)
				}));
	}

	public async listByPerson(
			event: IInternalEvent,
			person: IInternalPerson
	): Promise<IPersonData<DataI>[]> {
		const datas: IInternalPersonData<DataI>[] = await this.database.executeParams<IInternalPersonData<DataI>>(
				'PersonTracker__Data__LIST_BY_EVENT_AND_PERSON',
				{
						event: event.id,
						person: person.id
				}
		);

		return datas
				.map((d: IInternalPersonData<DataI>): IPersonData<DataI> => ({
						...d,
						value: decodeValue(d.value)
				}));
	}

	public async getByUid(uid: string): Promise<DataI|undefined> {
		const data: IInternalData<DataI>|undefined = await CommonsUniqueNamedHelper.build<IInternalData<DataI>>(this.database)
				.getFirstClassByName(
						this,
						uid,
						'uid'
				);
		
		if (!data) return undefined;

		return {
				...data,
				value: decodeValue(data.value)
		};
	}
	
	//-------------------------------------------------------------------------

	public async insertInteractionLogEntry(
			event: IInternalEvent,
			interaction: IInternalInteraction,
			field: IInternalField,
			timestamp: Date,
			value: number|string|boolean|Date|undefined,
			additionalValues: { [ key: string ]: unknown } = {}
	): Promise<IInteractionData<DataI>> {
		// we need this semaphore as the logno get is not transactional against the insert, so a key violation could occur if multiple transactions are running at once
		try {
			await this.lognoTransactionSemaphore.claim(2000);
		} catch (e) {
			// unable to claim semaphore in 2000ms.
			throw new Error('Unable to claim semaphore for logono insertInteractionLogEntry in 2000ms');
		}
		try {
			const logno: number = await this.getLogno(event) + 1;

			const data: Omit<
					IInternalInteractionData<DataI>,
					'event' | 'id'
			> = {
					uid: commonsBase62GenerateRandomId(),
					logno: logno,
					timestamp: timestamp,
					field: field.id,
					value: encodeValue(field, value),
					interaction: interaction.id,
					...additionalValues
			} as any;

			const inserted: IInternalInteractionData<DataI> = await this.insertForFirstClass(
					event,
					data
			) as IInternalInteractionData<DataI>;

			return {
					...inserted,
					value: decodeValue(inserted.value)
			};
		} finally {
			this.lognoTransactionSemaphore.release();
		}
	}

	public async insertPersonLogEntry(
			event: IInternalEvent,
			person: IInternalPerson,
			field: IInternalField,
			timestamp: Date,
			value: number|string|boolean|Date|undefined,
			additionalValues: { [ key: string ]: unknown } = {}
	): Promise<IPersonData<DataI>> {
		// we need this semaphore as the logno get is not transactional against the insert, so a key violation could occur if multiple transactions are running at once
		try {
			await this.lognoTransactionSemaphore.claim(2000);
		} catch (e) {
			// unable to claim semaphore in 2000ms.
			throw new Error('Unable to claim semaphore for logono insertInteractionLogEntry in 2000ms');
		}
		try {
			const logno: number = await this.getLogno(event) + 1;

			const data: Omit<
					IInternalPersonData<DataI>,
					'event' | 'id'
			> = {
					uid: commonsBase62GenerateRandomId(),
					logno: logno,
					timestamp: timestamp,
					field: field.id,
					value: encodeValue(field, value),
					person: person.id,
					...additionalValues
			} as any;

			const inserted: IInternalPersonData<DataI> = await this.insertForFirstClass(
					event,
					data
			) as IInternalPersonData<DataI>;

			return {
					...inserted,
					value: decodeValue(inserted.value)
			};
		} finally {
			this.lognoTransactionSemaphore.release();
		}
	}

	public async updateLogEntry(
			event: IEvent,
			field: IField,
			entry: DataI
	): Promise<true> {
		const data: IInternalData<DataI> = {
				...entry,
				value: encodeValue(field, entry.value)
		};

	 	await this.updateForFirstClass(
				event,
				data
		);

		return true;
	}
}
