
import { CommonsDatabaseTypeBigInt, CommonsDatabaseTypeDateTime, CommonsDatabaseTypeId, CommonsSqlDatabaseService, ECommonsDatabaseReferentialAction, ECommonsDatabaseTypeNull, ECommonsDatabaseTypeSigned, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsModelForeignKey, CommonsSecondClass } from 'nodecommons-es-models';

import { IData, IInteractionStep } from 'person-tracker-ts-assets';
import { CommonsSemaphore } from 'tscommons-es-async/dist';
import { DataModel, IInternalData } from './data.model';

import { InteractionModel, IInternalInteraction } from './interaction.model';
import { IInternalStep, StepModel } from './step.model';

export interface IInternalInteractionStep extends IInteractionStep {
		logno: number;	// some step changes could be sub-second timeframes, so we need an alternative secondary index for sub-second sorting
}

export class InteractionStepModel<DataI extends IData = IData> extends CommonsSecondClass<
		IInternalInteractionStep,
		IInternalInteraction
> {
	private lognoTransactionSemaphore: CommonsSemaphore = new CommonsSemaphore();
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			interactionModel: InteractionModel,
			stepModel: StepModel,
			dataModel: DataModel
	) {
		super(
				database,
				'interaction_steps',
				{
						step: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.NOT_NULL),
						timestamp: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
						logno: new CommonsDatabaseTypeBigInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
						
						// this isn't actually used at the moment, but could be in the future
						data: new CommonsDatabaseTypeId(ECommonsDatabaseTypeNull.ALLOW_NULL)
				},
				interactionModel,
				'interaction',
				{
						step: new CommonsModelForeignKey(
								stepModel,
								ECommonsDatabaseReferentialAction.CASCADE,
								ECommonsDatabaseReferentialAction.CASCADE
						),
						data: new CommonsModelForeignKey(
								dataModel,
								ECommonsDatabaseReferentialAction.SET_NULL,
								ECommonsDatabaseReferentialAction.CASCADE
						)
				}
		);
	}

	protected override preprepare(): void {
		super.preprepare();

		this.database.preprepare(
				'PersonTracker__InteractionStep__GET_LOGNO',
				`
					SELECT MAX(${this.modelField(this, 'logno')}) AS "logno"
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'interaction')} = :interaction
				`,
				{
						interaction: this.structure['interaction']
				},
				{
						logno: new CommonsDatabaseTypeBigInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.ALLOW_NULL)
				}
		);
	}
	
	public async getLogno(interaction: IInternalInteraction): Promise<number> {
		const logno: number|undefined = await this.database.executeParamsValue<number>(
				'PersonTracker__InteractionStep__GET_LOGNO',
				{
						interaction: interaction.id
				},
				true
		 );

		 return logno || 0;
	}
	
	public async listByInteraction(interaction: IInternalInteraction): Promise<IInternalInteractionStep[]> {
		return (await this.listByFirstClass(interaction))
				.sort((a: IInternalInteractionStep, b: IInternalInteractionStep): number => {
					if (a.timestamp.getTime() < b.timestamp.getTime()) return -1;
					if (a.timestamp.getTime() > b.timestamp.getTime()) return 1;
					if (a.logno < b.logno) return -1;
					if (a.logno > b.logno) return 1;

					return 0;
				});
	}

	public async createForInteraction(
			interaction: IInternalInteraction,
			step: IInternalStep,
			timestamp: Date
	): Promise<IInternalInteractionStep> {
		// we need this semaphore as the logno get is not transactional against the insert, so a key violation could occur if multiple transactions are running at once
		try {
			await this.lognoTransactionSemaphore.claim(2000);
		} catch (e) {
			// unable to claim semaphore in 2000ms.
			throw new Error('Unable to claim semaphore for logono insertInteractionLogEntry in 2000ms');
		}
		try {
			const logno: number = await this.getLogno(interaction) + 1;

			return await this.insertForFirstClass(
					interaction,
					{
							step: step.id,
							timestamp: timestamp,
							logno: logno
					}
			);
		} finally {
			this.lognoTransactionSemaphore.release();
		}
	}

	// These aren't be used at the moment. Future possibilities

	public async setData(
			interactionStep: IInternalInteractionStep,
			data: IInternalData<DataI>
	): Promise<void> {
		interactionStep.data = data.id;

		await this.update(interactionStep);
	}

	public async clearData(interactionStep: IInternalInteractionStep): Promise<void> {
		interactionStep.data = undefined;

		await this.update(interactionStep);
	}
}
