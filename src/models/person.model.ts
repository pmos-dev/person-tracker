import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeBase62BigId } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsUniqueNamedHelper } from 'nodecommons-es-models-adamantine';

import { IPerson } from 'person-tracker-ts-assets';

import { EventModel, IInternalEvent } from './event.model';

// ridiculous hack because TypeScript can't seem to realise that IField is already a subinterface of ICommonsManaged
export interface IInternalPerson extends IPerson { [ key: string ]: any }

export function toIPerson(person: IInternalPerson): IPerson {
	return { ...person };
}

export function toIInternalPerson(person: IPerson): IInternalPerson {
	return { ...person };
}

export class PersonModel extends CommonsSecondClass<
		IInternalPerson,
		IInternalEvent
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			eventModel: EventModel
	) {
		super(
				database,
				'persons',
				{
						uid: new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				eventModel,
				'event'
		);
	}
	
	protected override preprepare(): void {
		super.preprepare();
		
		CommonsUniqueNamedHelper.build<IInternalPerson>(this.database)
				.buildPreprepareForFirstClass(
						this,
						'uid',
						true
				);
	}

	public async getByUid(uid: string): Promise<IInternalPerson|undefined> {
		return CommonsUniqueNamedHelper.build<IInternalPerson>(this.database)
				.getFirstClassByName(
						this,
						uid,
						'uid'
				);
	}
	
	public async createForEvent(
			event: IInternalEvent
	): Promise<IInternalPerson> {
		return await super.insertForFirstClass(
				event,
				{
						uid: commonsBase62GenerateRandomId()
				}
		);
	}
}
