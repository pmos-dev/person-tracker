import { TDateRange } from 'tscommons-es-core';

import { CommonsDatabaseTypeString, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeDateTime } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsManagedSecondClass } from 'nodecommons-es-models-adamantine';

import { IEvent } from 'person-tracker-ts-assets';

import { NamespaceModel, IInternalNamespace } from './namespace.model';

// ridiculous hack because TypeScript can't seem to realise that IEvent is already a subinterface of ICommonsManaged
export interface IInternalEvent extends IEvent { [ key: string ]: any }

export class EventModel extends CommonsManagedSecondClass<
		IInternalEvent,
		IInternalNamespace
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			namespaceModel: NamespaceModel
	) {
		super(
				database,
				'events',
				{
						start: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
						end: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
						description: new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)
				},
				namespaceModel,
				'namespace',
				undefined,
				undefined,
				undefined,
				false,
				true
		);
	}

	protected override preprepare(): void {
		super.preprepare();

		this.database.preprepare(
				'PersonTracker__Event__LIST_BY_NAMESPACE_AND_DATERANGE',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'namespace')} = :namespace
					AND DATE(${this.modelField(this, 'start')}) <= :to
					AND DATE(${this.modelField(this, 'end')}) >= :from
				`,
				{
						namespace: this.structure['namespace'],
						from: this.structure['start'],
						to: this.structure['end']
				},
				this.structure
		);
	}
	
	//-------------------------------------------------------------------------
	
	public async listByNamespaceAndDateRange(
			namespace: IInternalNamespace,
			dateRange: TDateRange
	): Promise<IInternalEvent[]> {
		return await this.database.executeParams<IInternalEvent>(
				'PersonTracker__Event__LIST_BY_NAMESPACE_AND_DATERANGE',
				{
						namespace: namespace.id,
						from: dateRange.from,
						to: dateRange.to
				}
		 );
	}

}
