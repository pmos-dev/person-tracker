import { commonsBase62GenerateRandomId } from 'tscommons-es-core';

import { CommonsSqlDatabaseService, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsDatabaseTypeBase62BigId } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsSecondClass } from 'nodecommons-es-models';
import { CommonsUniqueNamedHelper } from 'nodecommons-es-models-adamantine';

import { IInteraction } from 'person-tracker-ts-assets';

import { EventModel, IInternalEvent } from './event.model';

// ridiculous hack because TypeScript can't seem to realise that IInteraction is already a subinterface of ICommonsManaged
export interface IInternalInteraction extends IInteraction { [ key: string ]: any }

export function toIInteraction(interaction: IInternalInteraction): IInteraction {
	return { ...interaction };
}

export function toIInternalInteraction(interaction: IInteraction): IInternalInteraction {
	return { ...interaction };
}

export class InteractionModel extends CommonsSecondClass<
		IInternalInteraction,
		IInternalEvent
> {
	
	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			eventModel: EventModel
	) {
		super(
				database,
				'interactions',
				{
						uid: new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL)
				},
				eventModel,
				'event'
		);
	}
	
	protected override preprepare(): void {
		super.preprepare();
		
		CommonsUniqueNamedHelper.build<IInternalInteraction>(this.database)
				.buildPreprepareForFirstClass(
						this,
						'uid',
						true
				);
	}

	public async getByUid(uid: string): Promise<IInternalInteraction|undefined> {
		return CommonsUniqueNamedHelper.build<IInternalInteraction>(this.database)
				.getFirstClassByName(
						this,
						uid,
						'uid'
				);
	}
	
	public async createForEvent(
			event: IInternalEvent
	): Promise<IInternalInteraction> {
		return await super.insertForFirstClass(
				event,
				{
						uid: commonsBase62GenerateRandomId()
				}
		);
	}
}
