import { CommonsDatabaseTypeString, ICommonsCredentials } from 'nodecommons-es-database';
import { CommonsSqlDatabaseService } from 'nodecommons-es-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-es-database';
import { CommonsManagedSecondClass } from 'nodecommons-es-models-adamantine';

import { ITriage } from 'person-tracker-ts-assets';

import { NamespaceModel, IInternalNamespace } from './namespace.model';

// ridiculous hack because TypeScript can't seem to realise that IField is already a subinterface of ICommonsManaged
export interface IInternalTriage extends ITriage { [ key: string ]: any }

export class TriageModel extends CommonsManagedSecondClass<
		IInternalTriage,
		IInternalNamespace
> {

	constructor(
			database: CommonsSqlDatabaseService<ICommonsCredentials>,
			namespaceModel: NamespaceModel
	) {
		super(
				database,
				'triages',
				{
						description: new CommonsDatabaseTypeString(255, ECommonsDatabaseTypeNull.NOT_NULL)
				},
				namespaceModel,
				'namespace',
				undefined,
				undefined,
				undefined,
				false,
				true
		);
	}

}
